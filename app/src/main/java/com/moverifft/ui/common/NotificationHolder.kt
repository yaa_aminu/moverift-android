package com.moverifft.ui.common

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import com.moverifft.R

/**
 * Created by yaaminu on 10/29/17.
 */

class NotificationHolder(v: View) : RecyclerViewBaseAdapter.Holder(v) {
    @BindView(R.id.user_avatar)
    lateinit var userAvatar: ImageView
    @BindView(R.id.notification_details)
    lateinit var notificationText: TextView
    @JvmField
    @BindView(R.id.tv_notification_date)
    var notificationDate: TextView? = null
    @JvmField
    @BindView(R.id.title)
    var title: TextView? = null

    @JvmField
    @BindView(R.id.sub_title)
    var subTitle: TextView? = null
}
