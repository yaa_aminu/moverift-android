package com.moverifft.ui.common;

import android.text.format.DateUtils;
import android.view.ViewGroup;

import com.moverifft.R;
import com.moverifft.ui.clients.Trip;
import com.squareup.picasso.Picasso;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripsRecyclerViewAdapter extends RecyclerViewBaseAdapter<Trip, TripHolder> {

    private final Delegate delegate;

    public TripsRecyclerViewAdapter(Delegate delegate) {
        super(delegate);
        this.delegate = delegate;
    }

    @Override
    protected void doBindHolder(TripHolder holder, int position) {
        Trip item = getItem(position);
        holder.tripName.setText(item.getDriverName());
        holder.tv_vehicle_no.setText(item.getVehicleNo());
        holder.tv_start_date.setText(DateUtils.formatDateTime(delegate.context(),
                item.getStartTime(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_ABBREV_ALL));
        Picasso.with(delegate.context())
                .load(item.getDriverDp())
                .placeholder(R.drawable.ic_person_white_24dp)
                .error(R.drawable.ic_person_white_24dp)
                .into(holder.driverAvatar);
    }

    @Override
    public TripHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TripHolder(inflater.inflate(R.layout.trip_list_item, parent, false));
    }

    public interface Delegate extends RecyclerViewBaseAdapter.Delegate<Trip> {


    }

}
