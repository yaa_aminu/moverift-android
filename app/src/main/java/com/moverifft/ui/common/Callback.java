package com.moverifft.ui.common;

/**
 * Created by yaaminu on 9/18/17.
 */

public interface Callback<T> {

    void done(Exception e, T t);
}
