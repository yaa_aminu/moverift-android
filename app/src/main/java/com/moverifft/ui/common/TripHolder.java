package com.moverifft.ui.common;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.moverifft.R;

import butterknife.BindView;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripHolder extends RecyclerViewBaseAdapter.Holder {
    @BindView(R.id.tv_date_started)
    TextView tv_start_date;
    @BindView(R.id.tv_vehicle_no)
    TextView tv_vehicle_no;
    @BindView(R.id.tv_trip_name)
    TextView tripName;
    @BindView(R.id.user_avatar)
    ImageView driverAvatar;

    public TripHolder(View v) {
        super(v);
    }
}


