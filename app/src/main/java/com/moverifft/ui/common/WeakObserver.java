package com.moverifft.ui.common;

import java.lang.ref.WeakReference;

import rx.Observer;

/**
 * Created by yaaminu on 12/30/17.
 */

public class WeakObserver<T> implements Observer<T> {

    private final WeakReference<Observer<T>> weakReference;

    public WeakObserver(Observer<T> observer) {
        weakReference = new WeakReference<>(observer);
    }

    @Override
    public void onCompleted() {
        Observer<T> tObserver = weakReference.get();
        if (tObserver != null) {
            weakReference.get().onCompleted();
        }
    }

    @Override
    public void onError(Throwable e) {
        Observer<T> tObserver = weakReference.get();
        if (tObserver != null) {
            tObserver.onError(e);
        }
    }

    @Override
    public void onNext(T t) {
        Observer<T> tObserver = weakReference.get();
        if (tObserver != null) {
            tObserver.onNext(t);
        }
    }
}
