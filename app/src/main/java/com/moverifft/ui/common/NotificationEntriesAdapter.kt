package com.moverifft.ui.common

import android.view.ViewGroup
import com.moverifft.ui.driver.NotificationEntry

/**
 * Created by yaaminu on 10/29/17.
 */

class NotificationEntriesAdapter(delegate: RecyclerViewBaseAdapter.Delegate<NotificationEntry>)
    : RecyclerViewBaseAdapter<NotificationEntry, NotificationHolder>(delegate) {

    override fun doBindHolder(holder: NotificationHolder, position: Int) {
        getItem(position).updateView(holder)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationHolder {
        return NotificationHolder(inflater.inflate(NotificationEntry.getLayout(viewType), parent, false))
    }

}
