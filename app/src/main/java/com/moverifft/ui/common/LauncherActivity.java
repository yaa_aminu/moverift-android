package com.moverifft.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.moverifft.MoveRifft;
import com.moverifft.ui.clients.ClientMainActivity;
import com.moverifft.ui.driver.BaseActivity;
import com.moverifft.ui.driver.LoginActivity;
import com.moverifft.users.data.UserApi;

import javax.inject.Inject;

/**
 * Created by yaaminu on 8/17/17.
 */

public class LauncherActivity extends BaseActivity {
    @Inject
    UserApi userApi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MoveRifft.depGraph.inject(this);
        if (userApi.isCurrentUserSetup()) {
            startActivity(new Intent(this, ClientMainActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }
}
