package com.moverifft.ui.clients

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.moverifft.ui.common.RecyclerViewBaseAdapter

/**
 * Created by yaaminu on 1/9/18.
 */

class PlacesAdapter(delegate: RecyclerViewBaseAdapter.Delegate<LatLang>)
    : RecyclerViewBaseAdapter<LatLang, PlaceHolder>(delegate) {

    override fun doBindHolder(holder: PlaceHolder?, position: Int) {
        (holder!!.itemView as TextView).text = getItem(position).name
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlaceHolder {
        return PlaceHolder(inflater.inflate(android.R.layout.simple_list_item_1, parent, false))
    }

}

class PlaceHolder(view: View) : RecyclerViewBaseAdapter.Holder(view)