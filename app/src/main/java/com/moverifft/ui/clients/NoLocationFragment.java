package com.moverifft.ui.clients;

import android.content.Intent;
import android.provider.Settings;

import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;

import butterknife.OnClick;

/**
 * Created by yaaminu on 10/31/17.
 */

public class NoLocationFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.location_required_layout;
    }

    @OnClick(R.id.bt_turn_on_location)
    void turnOnLocation() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }
}
