package com.moverifft.ui.clients

import android.os.Bundle
import android.view.View
import com.moverifft.R
import com.moverifft.truck.data.Truck
import kotlinx.android.synthetic.main.choose_truck_type_fragment.*

/**
 * Created by yaaminu on 1/7/18.
 */


class ChooseTruckTypeFragment : WizardFragment(), TruckTypeFragmentSection.OnTruckTypeSelected {
    override fun onTruckTypeSelected(type: Int) {
        listener.request().estimatedCost = 1200L
        listener.request().truck = Truck("", "", 0, type, "",
                "", false, false, null, "")
        listener.next(this@ChooseTruckTypeFragment)
    }

    override fun getLayout(): Int {
        return R.layout.choose_truck_type_fragment
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagerAdapter = PagerAdapter(childFragmentManager)
        pager.adapter = pagerAdapter
        tab_layout.setupWithViewPager(pager)
    }

}