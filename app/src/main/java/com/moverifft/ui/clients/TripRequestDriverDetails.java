package com.moverifft.ui.clients;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.utils.GenericUtils;

import butterknife.BindView;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripRequestDriverDetails extends BaseFragment {

    public static final String ARG_DISTANCE_APART = "distanceApart";
    public static final String ARG_DRIVER_NAME = "driverName";
    public static final String ARG_TRUCK_REG_NO = "truckType";

    @Override
    protected int getLayout() {
        return R.layout.layout_driver_details;
    }

    @BindView(R.id.tv_distance_away)
    TextView tv_distance_away;
    @BindView(R.id.driver_name)
    TextView driver_name;
    @BindView(R.id.tv_vehicle_no)
    TextView tv_vehicle_no;


    public static TripRequestDriverDetails create(double distanceApart, @NonNull String driverName,
                                                  @NonNull String vehicleNo) {
        Bundle bundle = new Bundle(3);
        bundle.putDouble(ARG_DISTANCE_APART, distanceApart);
        bundle.putString(ARG_DRIVER_NAME, driverName);
        bundle.putString(ARG_TRUCK_REG_NO, vehicleNo);
        TripRequestDriverDetails tripRequestDriverDetails = new TripRequestDriverDetails();
        tripRequestDriverDetails.setArguments(bundle);
        return tripRequestDriverDetails;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        double distanceAway = getArguments().getDouble(ARG_DISTANCE_APART);
        String driverName = getArguments().getString(ARG_DRIVER_NAME);
        String truckRegNo = getArguments().getString(ARG_TRUCK_REG_NO);
        tv_distance_away.setText(normaliseDistance(distanceAway));
        tv_vehicle_no.setText(Html.fromHtml(getString(R.string.vehicle_no, "<b>" + truckRegNo + "</b>")));
        driver_name.setText(driverName);
    }

    private String normaliseDistance(double distance) {
        if (distance > 1000) {
            return getString(R.string.distance_apart,
                    GenericUtils.formatDistance(distance / 1000) + " Km");
        }
        return getString(R.string.distance_apart, GenericUtils.formatDistance(distance) + " metres");
    }
}
