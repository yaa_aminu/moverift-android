package com.moverifft.ui.clients

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.TaskStackBuilder
import android.view.View
import com.moverifft.MoveRifft
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.users.data.UserManager
import com.moverifft.users.net.OrderDetailsActivity
import com.moverifft.utils.UiHelpers
import org.jetbrains.anko.support.v4.toast
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by yaaminu on 1/7/18.
 */


class BookTruckFragment : BaseFragment(), WizardFragment.WizardFragmentListener {

    @Inject
    lateinit var tripsApi: TripsApi

    val request = TripRequest()


    private val pickupDestPickerFragment = PickupDestPickerFragment()

    private val chooseTruckTypeFragment = ChooseTruckTypeFragment()

    private val confirmBookingFragment = ConfirmBookingFragment()

    private val goodsTypeFragment = GoodsTypeFragment()

    override fun next(wizardFragment: WizardFragment) {
        when (wizardFragment) {
            is GoodsTypeFragment -> replaceWith(pickupDestPickerFragment)
            is PickupDestPickerFragment -> replaceWith(chooseTruckTypeFragment)
            is ChooseTruckTypeFragment -> replaceWith(confirmBookingFragment)
            is ConfirmBookingFragment -> submitBooking()
        }
    }


    private fun submitBooking() {
        if (validate()) {
            val dialog = UiHelpers.showProgressDialog(context)
            tripsApi.bookTrip(Order(request))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        dialog.dismiss()
                        val stack = TaskStackBuilder.create(context)
                        val intent = Intent(context, OrderDetailsActivity::class.java)
                        intent.putExtra(OrderDetailsActivity.EXTRA_ORDER, it)
                        stack.addNextIntent(Intent(context, ClientMainActivity::class.java))
                        stack.addNextIntent(Intent(context, TripsActivity::class.java))
                        stack.addNextIntent(intent)
                        stack.startActivities()
                        activity?.finish()
                    }, {
                        dialog.dismiss()
                        UiHelpers.showErrorDialog(context, it.message)
                    })

        } else {
            toast("validation failed")
        }
    }

    private fun validate() = request.startLocation != null && request.endLocation != null && request.truck != null

    private fun replaceWith(wizardFragment: WizardFragment) {
        childFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, wizardFragment, wizardFragment::class.java.simpleName)
                .commit()
    }

    private fun goBack(wizardFragment: WizardFragment): Boolean {
        return when (wizardFragment) {
            is GoodsTypeFragment -> false
            is PickupDestPickerFragment -> {
                replaceWith(goodsTypeFragment)
                true
            }
            is ChooseTruckTypeFragment -> {
                replaceWith(pickupDestPickerFragment)
                true
            }
            is ConfirmBookingFragment -> {
                replaceWith(chooseTruckTypeFragment)
                true
            }
            else -> throw IllegalArgumentException("unknown stage")
        }
    }

    override fun request(): TripRequest {
        return request
    }

    override fun getLayout(): Int {
        return R.layout.fragment_book_truck
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.request.clientId = UserManager.getInstance().getCurrentUser()!!.userId
        MoveRifft.depGraph.inject(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        replaceWith(goodsTypeFragment)
    }


    override fun onBackPressed(): Boolean {
        val currentFragment = childFragmentManager.findFragmentById(R.id.fragment_container)
        return if (currentFragment is WizardFragment && goBack(currentFragment)) {
            true
        } else {
            super.onBackPressed()
        }
    }

}