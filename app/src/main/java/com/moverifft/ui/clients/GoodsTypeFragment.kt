package com.moverifft.ui.clients

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import butterknife.OnEditorAction
import com.moverifft.R
import kotlinx.android.synthetic.main.fragment_goods_type.*
import java.util.concurrent.TimeUnit

/**
 * Created by yaaminu on 1/7/18.
 */


class GoodsTypeFragment : WizardFragment() {

    private lateinit var hintArray: Array<String>

    override fun getLayout(): Int {
        return R.layout.fragment_goods_type
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hintArray = resources.getStringArray(R.array.hint_types)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        continue_.setOnClickListener {
            if (validate()) {
                listener.request().goodsType = et_goods_type.text.toString().trim()
                listener.request().notes = et_notes.text.toString().trim()
                listener.next(this@GoodsTypeFragment)
            }
        }


    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(updateGoodsType, TimeUnit.SECONDS.toMillis(5))
    }

    override fun onPause() {
        handler.removeCallbacks(updateGoodsType)
        super.onPause()
    }

    val handler = Handler(Looper.getMainLooper())

    var currentHint = 1
    var updateGoodsType = object : Runnable {
        override fun run() {
            if (activity == null) return
            if (currentHint >= hintArray.size) {
                currentHint = 0
            }
            et_goods_type.hint = hintArray[currentHint]
            currentHint++
            handler.removeCallbacks(this)
            handler.postDelayed(this, TimeUnit.SECONDS.toMillis(5))
        }
    }

    @OnEditorAction(R.id.et_notes)
    fun onEditorAction(): Boolean {
        continue_.performClick()
        return true
    }

    private fun validate(): Boolean {
        return if (et_goods_type.text.isBlank()) {
            et_goods_type.error = getString(R.string.error_goods_type_not_specified)
            false
        } else if (et_notes.text.isBlank()) {
            et_notes.error = getString(R.string.notes_required)
            false
        } else {
            true
        }
    }

}