package com.moverifft.ui.clients

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.text.Editable
import butterknife.OnClick
import butterknife.OnTextChanged
import com.google.android.gms.maps.model.Circle
import com.moverifft.R
import com.moverifft.utils.PLog
import kotlinx.android.synthetic.main.fragment_pickup_dest_picker_fragment.*

/**
 * Created by yaaminu on 1/7/18.
 */

const val TAG = "PickUpDestPickerFragment"

class PickupDestPickerFragment : WizardFragment(),
        PlacesAutoCompleteFragment.OnPlaceSelectedCallback, LocationListener {

    override fun onLocationChanged(location: Location?) {
        PLog.d(TAG, "location available $location")
        if (location != null) {
            currentLocation = LatLang(location.latitude, location.longitude)
            listener.request().setCurrenLocation(currentLocation)
        }
    }

    private lateinit var locationManager: LocationManager
    private var currentLocation: LatLang? = null

    override fun onPlaceSelected(latLang: LatLang) {
        PLog.d(TAG, "selected place: $latLang")
        when (choose_location.tag) {
            null -> listener.request().startLocation = latLang
            else -> listener.request().endLocation = latLang
        }
        chooseLocation()
    }

    override fun onNothingRetrieved() {
        PLog.d(TAG, "nothing found")
//        when (choose_location.tag) {
//            null -> listener.request().startLocation = null
//            else -> listener.request().endLocation = null
//        }
//        refreshDisplay()
    }


    private var you: Circle? = null

    override fun getLayout(): Int {
        return R.layout.fragment_pickup_dest_picker_fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        //we must be having permission here if not crash
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 20f, this)
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 20f, this)
    }

    override fun onPause() {
        locationManager.removeUpdates(this)
        you = null
        super.onPause()
    }

    @OnTextChanged(R.id.et_location)
    fun onLocationChanged(text: Editable) {
        if (et_location.tag != null) {
            et_location.tag = null
        } else {
            if (choose_location.tag == null) {
                listener.request().startLocation = null
            } else {
                listener.request().endLocation = null
            }
            et_location.removeCallbacks(updateLocationName)
            if (text.trim().isNotEmpty()) {
                et_location.postDelayed(updateLocationName, 1000)
            }
        }
    }

    @OnClick(R.id.choose_location)
    fun chooseLocation() {
        val location = et_location.text.toString().trim()
        if (choose_location.tag == null) {
            if (listener.request().startLocation == null) {
                when {
                    location.isEmpty() -> {
                        et_location.error = getString(R.string.error_pickup_location_required)
                        return
                    }
                    location.length < 15 -> {
                        et_location.error = getString(R.string.address_too_short)
                        return
                    }
                    else -> {
                        listener.request().startLocation = LatLang(-1.0, -1.0, location)
                    }
                }
            }
            choose_location.tag = "next"
            choose_location.text = getString(R.string.set_destination)
            et_location.hint = getString(R.string.destination)
            et_location.setText("")
        } else {
            //TODO check if the selected location is supported
            if (listener.request().endLocation == null) {
                when {
                    location.isEmpty() -> {
                        et_location.error = getString(R.string.error_destination_required)
                        return
                    }
                    location.length < 15 -> {
                        et_location.error = getString(R.string.address_too_short)
                        return
                    }
                    else -> {
                        listener.request().endLocation = LatLang(-1.0, -1.0, location)
                    }
                }
            }
            listener.next(this)
        }
    }

    private val updateLocationName = {
        val fr = childFragmentManager.findFragmentById(R.id.places_auto_complete) as PlacesAutoCompleteFragment
        val str = et_location.text.toString().trim()
        if (str.isNotEmpty()) {
            fr.search(str, currentLocation)
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

}
