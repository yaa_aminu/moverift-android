package com.moverifft.ui.clients

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.moverifft.MoveRifft
import com.moverifft.R
import com.moverifft.ui.driver.BaseActivity
import com.moverifft.users.net.PendingOrderFragment
import com.moverifft.utils.GenericUtils
import kotlinx.android.synthetic.main.activity_trips.*
import javax.inject.Inject

/**
 * Created by yaaminu on 8/24/17.
 */

class TripsActivity : BaseActivity(), TripsFragment.TripFragmentListener {


    @Inject
    lateinit var tripsApi: TripsApi
    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoveRifft.depGraph.inject(this)
        setContentView(R.layout.activity_trips)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle(R.string.my_trips)
        pager.adapter = PagerAdapter(supportFragmentManager,
                PendingOrderFragment(), TripsFragment(), TripsFragment())
        tab_layout.setupWithViewPager(pager)
    }

    override fun onTripSelected(trip: Trip) {
        val intent = Intent(this, TripDetailsActivity::class.java)
        intent.putExtra(TripDetailsActivity.EXTRA_TRIP, trip)
        startActivity(intent)
    }


    class PagerAdapter(fragmentManager: FragmentManager, private val pendingOrderFragment: PendingOrderFragment,
                       private val tripsFragment: TripsFragment, private val tripsFragment2: TripsFragment) : FragmentPagerAdapter(fragmentManager) {
        override fun getItem(position: Int) = when (position) {
            0 -> pendingOrderFragment
            1 -> tripsFragment
            2 -> tripsFragment2
            else -> throw AssertionError()
        }

        override fun getCount() = 3

        override fun getPageTitle(position: Int): CharSequence = when (position) {
            0 -> GenericUtils.getString(R.string.orders)
            1 -> GenericUtils.getString(R.string.ongoing)
            2 -> GenericUtils.getString(R.string.past)
            else -> throw AssertionError()
        }
    }
}
