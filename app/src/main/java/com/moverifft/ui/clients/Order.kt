package com.moverifft.ui.clients

import android.os.Parcel
import android.os.Parcelable
import com.parse.ParseObject
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by yaaminu on 1/16/18.
 */

const val PAY_ON_PICKUP = 1
const val PAY_ON_DELIVERY = 2

open class Order(@PrimaryKey var orderId: String,
                 var orderNumber: String?,
                 var customerId: String, var date: Long,
                 var pickup: LatLang?, var destination: LatLang?,
                 var truckType: Int, var goods: String, var notes: String = "",
                 var estimatedCost: Long = 0, var paymentMode: Int = PAY_ON_PICKUP, private var processed: Boolean = false) : RealmObject(), Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readParcelable(LatLang::class.java.classLoader),
            parcel.readParcelable(LatLang::class.java.classLoader),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readInt(),
            parcel.readByte() == 1.toByte())

    constructor() : this("", "", "", 0L, null, null, -1, "")
    constructor(req: TripRequest) : this("", "", req.clientId, System.currentTimeMillis(),
            req.startLocation, req.endLocation, req.truck!!.truckTypeIdentifier,
            req.goodsType, req.notes, req.estimatedCost)

    companion object {
        @JvmStatic
        fun create(obj: ParseObject): Order {
            val order = Order()
            return order.apply {
                orderId = obj.objectId
                customerId = obj.getString("customerId")
                date = obj.createdAt.time
                pickup = LatLang.fromJson(obj.getString("pickUp"))
                destination = LatLang.fromJson(obj.getString("destination"))
                truckType = obj.getInt("truckType")
                goods = obj.getString("goods")
                notes = obj.getString("notes")
                orderNumber = obj.getString("orderNumber")
                paymentMode = obj.getInt("paymentMode")
                estimatedCost = obj.getLong("estimatedCost")
                processed = obj.getBoolean("processed")
            }
        }

        @JvmField
        val CREATOR = object : Parcelable.Creator<Order> {
            override fun createFromParcel(parcel: Parcel): Order {
                return Order(parcel)
            }

            override fun newArray(size: Int): Array<Order?> {
                return arrayOfNulls(size)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(orderId)
        parcel.writeString(customerId)
        parcel.writeString(orderNumber)
        parcel.writeLong(date)
        parcel.writeParcelable(pickup, flags)
        parcel.writeParcelable(destination, flags)
        parcel.writeInt(truckType)
        parcel.writeString(goods)
        parcel.writeString(notes)
        parcel.writeInt(paymentMode)
        parcel.writeLong(estimatedCost)
        parcel.writeByte(if (processed) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

}