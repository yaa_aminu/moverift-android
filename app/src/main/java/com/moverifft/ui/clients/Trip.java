package com.moverifft.ui.clients;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.moverifft.common.Constants;
import com.parse.ParseObject;

import org.json.JSONException;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;

import static com.moverifft.common.Constants.END_LOCATION;
import static com.moverifft.common.Constants.HIRED_BY;
import static com.moverifft.common.Constants.INITIAL_TRUCK_LOCATION;
import static com.moverifft.common.Constants.TRIP_END_TIME;

/**
 * Created by yaaminu on 8/22/17.
 */

public class Trip implements Parcelable, RealmModel {
    @PrimaryKey
    private String tripId;
    private LatLang startLocation;
    private LatLang endLocation;
    private String truckId;

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private String driverId;

    private long startTime;
    private long endTime;
    private String hiredBy;
    private LatLang initialTruckLocation;
    private String driverDp, driverName, vehicleNo;

    public Trip() {
    }

    public Trip(String tripId, String truckId, String hiredBy,
                LatLang initialTruckLocation,
                long startTime, LatLang startLocation,
                long endTime, @NonNull LatLang endLocation, @NonNull String driverId,
                String driverDp, String driverName, String vehicleNo) {
        this.tripId = tripId;
        this.truckId = truckId;
        this.hiredBy = hiredBy;
        this.startTime = startTime;
        this.initialTruckLocation = initialTruckLocation;
        this.startLocation = startLocation;
        this.endTime = endTime;
        this.endLocation = endLocation;
        this.driverId = driverId;
        this.driverDp = driverDp;
        this.driverName = driverName;
        this.vehicleNo = vehicleNo;
    }

    protected Trip(Parcel in) {
        tripId = in.readString();
        startLocation = in.readParcelable(LatLang.class.getClassLoader());
        endLocation = in.readParcelable(LatLang.class.getClassLoader());
        initialTruckLocation = in.readParcelable(LatLang.class.getClassLoader());
        truckId = in.readString();
        startTime = in.readLong();
        endTime = in.readLong();
        hiredBy = in.readString();
        driverDp = in.readString();
        driverName = in.readString();
        vehicleNo = in.readString();
    }

    public static final Creator<Trip> CREATOR = new Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };

    public static Trip create(ParseObject parseObject) {
        LatLang endLocation, initialTruckLocation, startLocation;

        try {
            endLocation = LatLang.fromJson(parseObject.getString(END_LOCATION));
            initialTruckLocation = LatLang.fromJson(parseObject.getString(INITIAL_TRUCK_LOCATION));
            startLocation = LatLang.fromJson(parseObject.getString(Constants.START_LOCATION));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return new Trip(parseObject.getObjectId(), parseObject.getString(Constants.TRUCK_ID),
                parseObject.getString(HIRED_BY),
                initialTruckLocation,
                parseObject.getCreatedAt().getTime(), startLocation,
                parseObject.getLong(TRIP_END_TIME), endLocation, parseObject.getString("driverId"),
                parseObject.getString("driverDp"), parseObject.getString("driverName"),
                parseObject.getString("vehicleNo"));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tripId);
        parcel.writeParcelable(startLocation, i);
        parcel.writeParcelable(endLocation, i);
        parcel.writeParcelable(initialTruckLocation, i);
        parcel.writeString(truckId);
        parcel.writeLong(startTime);
        parcel.writeLong(endTime);
        parcel.writeString(hiredBy);
        parcel.writeString(driverDp);
        parcel.writeString(driverName);
        parcel.writeString(vehicleNo);
    }

    public String getDriverDp() {
        return driverDp;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "tripId='" + tripId + '\'' +
                ", startLocation=" + startLocation +
                ", endLocation=" + endLocation +
                ", truckId='" + truckId + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", initialTruckLocation=" + initialTruckLocation +
                ", hiredBy='" + hiredBy + '\'' +
                '}';
    }

    public String getTruckId() {
        return truckId;
    }

    public LatLang getStartLocation() {
        return startLocation;
    }

    public LatLang getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(@NonNull LatLang endLocation) {
        this.endLocation = endLocation;
    }

    public long getStartTime() {
        return startTime;
    }

    public String getTripId() {
        return tripId;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getHiredBy() {
        return hiredBy;
    }
}
