package com.moverifft.ui.clients;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.moverifft.MoveRifft;
import com.moverifft.R;
import com.moverifft.truck.data.TruckApi;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.ui.common.RecyclerViewBaseAdapter;
import com.moverifft.ui.common.TripsRecyclerViewAdapter;
import com.moverifft.users.data.UserApi;
import com.moverifft.utils.GenericUtils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.moverifft.utils.ViewUtils.showByFlag;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripsFragment extends BaseFragment {

    @Nullable
    private Subscription subscription;

    @Override
    protected int getLayout() {
        return R.layout.fragment_trips;
    }

    @Inject
    TripsApi tripsApi;
    @Inject
    TruckApi truckApi;
    @Inject
    UserApi userApi;

    @BindView(R.id.trips_list)
    RecyclerView tripsList;
    @BindView(R.id.pb_loading)
    ProgressBar pb_loading;
    @BindView(R.id.empty_text)
    TextView empty_text;
    private boolean loading = true;


    String currentUserId;

    List<Trip> trips = Collections.emptyList();

    TripsRecyclerViewAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        GenericUtils.assertThat(getActivity() instanceof TripFragmentListener,
                "must implement " + TripFragmentListener.class.getName());
        super.onCreate(savedInstanceState);
        MoveRifft.depGraph.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loading = true;
        adapter = new TripsRecyclerViewAdapter(delegate);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        tripsList.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        tripsList.setLayoutManager(layoutManager);
        tripsList.setAdapter(adapter);

        //noinspection ConstantConditions
        currentUserId = userApi.getCurrentUser().getUserId();
        subscription = tripsApi.tripsForCurrentUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override
    public void onDestroyView() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        super.onDestroyView();
    }


    private final Observer<List<Trip>> observer = new Observer<List<Trip>>() {
        @Override
        public void onCompleted() {
            subscription = null;
            loading = false;
            if (!isDestroyed()) {
                adapter.notifyDataChanged();
            }
        }

        @Override
        public void onError(Throwable e) {
            loading = false;
            subscription = null;
            if (!isDestroyed()) {
                GenericUtils.showDialog(getContext(), e.getMessage());
                adapter.notifyDataChanged();
            }
        }

        @Override
        public void onNext(final List<Trip> trips) {
            loading = false;
            if (!isDestroyed()) {
                TripsFragment.this.trips = trips;
                adapter.notifyDataChanged();
            }
        }
    };


    private final TripsRecyclerViewAdapter.Delegate delegate = new TripsRecyclerViewAdapter.Delegate() {


        @Override
        public void onItemClick(RecyclerViewBaseAdapter<Trip, ?> adapter, View view, int position, long id) {
            adapter.notifyItemChanged(position);
            ((TripFragmentListener) getActivity())
                    .onTripSelected(adapter.getItem(position));
        }

        @Override
        public boolean onItemLongClick(RecyclerViewBaseAdapter<Trip, ?> adapter, View view, int position, long id) {
            return true;
        }

        @NonNull
        @Override
        public List<Trip> dataSet() {
            showByFlag(loading && trips.isEmpty(), pb_loading);
            showByFlag(!trips.isEmpty(), tripsList);
            showByFlag(!loading && trips.isEmpty(), empty_text);
            return trips;
        }

        @NonNull
        @Override
        public Context context() {
            return getContext();
        }
    };


    public interface TripFragmentListener {
        void onTripSelected(Trip trip);
    }
}
