package com.moverifft.ui.clients

import android.location.Address
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import com.moverifft.users.net.ParseBackend
import io.realm.Realm
import rx.Observable

/**
 * Created by yaaminu on 8/22/17.
 */

class TripsManager(private val backend: ParseBackend) : TripsApi {

    override fun trips(truckId: String): Observable<List<Trip>> {
        throw UnsupportedOperationException()
    }

    override fun findTripById(tripId: String): Observable<Trip> {
        return backend.findTripById(tripId)
    }

    override fun tripsForCurrentUser(): Observable<List<Trip>> {
        return backend.findTripsForCurrentUser()
    }

    override fun getRoutes(from: LatLang, to: LatLang): Observable<TripStatsAndRoutes> {
        return backend.getRoutes(from, to)
                .map({
                    val polyLines = it.getJSONArray("points")
                    val bounds = it.getJSONArray("bounds")
                    val ret = TripStatsAndRoutes(it.getDouble("distance"), it.getLong("duration"), emptyList(),
                            listOf(LatLng(bounds.getJSONObject(0).getDouble("lat"),
                                    bounds.getJSONObject(0).getDouble("lng")),
                                    LatLng(bounds.getJSONObject(1).getDouble("lat"),
                                            bounds.getJSONObject(1).getDouble("lng"))))

                    val decoded = ArrayList<LatLng>(polyLines.length())
                    for (i in 0.until(polyLines.length())) {
                        decoded.addAll(PolyUtil.decode(polyLines.getString(i)))
                    }
                    ret.route = decoded
                    ret
                })
    }

    override fun startTrip(hiredBy: String,
                           initialTruckLocation: LatLang,
                           startLocation: LatLang,
                           endLocation: LatLang, goodsType: String): Observable<Trip> {
        return backend.startTrip(hiredBy, initialTruckLocation, startLocation, endLocation, goodsType)
    }

    override fun endTrip(trip: Trip): Observable<Trip> {
        return backend.endTrip(trip)
    }

    override fun initiateTrip(tripRequest: TripRequest): Observable<Any> {
        throw UnsupportedOperationException()
    }

    override fun bookTrip(order: Order): Observable<Order> {
        return backend.initiateTrip(order)
                .map {
                    val realm = Realm.getDefaultInstance()
                    val ret = it
                    realm.use {
                        realm.executeTransaction {
                            realm.copyToRealmOrUpdate(ret)
                        }
                    }
                    ret
                }
    }

    override fun rejectTripRequest(userId: String, goodsType: String, endLocation: LatLang): Observable<*> {
        return backend.rejectRequest(userId, goodsType, endLocation)
    }

    override fun getLocationName(geoCoordinate: LatLang): Observable<Address> {
        return backend.getLocationName(geoCoordinate)
    }

    override fun trail(tripId: String): Observable<List<TripTrail>> {
        return backend.getTrail(tripId)
    }

    override fun loadOrders(): Observable<List<Order>> {
        return backend.loadOrders()
                .map {
                    val ret: List<Order> = it
                    val realm = Realm.getDefaultInstance()
                    realm.use {
                        realm.executeTransaction {
                            realm.copyToRealmOrUpdate(ret)
                        }
                    }
                    ret
                }
    }

    override fun getCurrentTrip(): Observable<Trip> {
        throw UnsupportedOperationException()
    }

    override fun getTripCount(): Observable<Int> {
        throw UnsupportedOperationException()
    }

    companion object {
        val TRIP_REQUEST_PREFS = "trip.request.prefs"
    }
}
