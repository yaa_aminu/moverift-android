package com.moverifft.ui.clients;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.moverifft.R;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripInfoFragmentClient extends InfoFragment {


    @Override
    protected int getLayout() {
        return R.layout.trip_info_layout;
    }


    public static TripInfoFragmentClient create(@Nullable Trip trip) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(ARG_TRIP, trip);
        TripInfoFragmentClient tripInfoFragmentClient = new TripInfoFragmentClient();
        tripInfoFragmentClient.setArguments(bundle);
        return tripInfoFragmentClient;
    }


}
