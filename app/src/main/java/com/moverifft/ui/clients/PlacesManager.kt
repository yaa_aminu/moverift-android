package com.moverifft.ui.clients

import com.moverifft.users.net.ParseBackend
import com.moverifft.users.net.PlacesHelper
import com.moverifft.utils.TaskManager
import rx.Observable
import java.io.IOException

/**
 * Created by yaaminu on 1/9/18.
 */

object PlacesManager {

    fun searchPlace(name: String, currentLocation: LatLang?): Observable<List<LatLang>> {
        return ParseBackend.getInstance()
                .searchPlace(name, currentLocation)
    }

    fun fetchPlaceDetails(location: LatLang, cb: (latLang: LatLang?, error: Exception?) -> Unit) {
        TaskManager.execute({
            try {
                val latLng = PlacesHelper
                        .findPlaceById(location.placeId!!)
                TaskManager.executeOnMainThread({
                    cb(LatLang(latLng.first, latLng.second, location.name), null)
                })
            } catch (e: IOException) {
                TaskManager.executeOnMainThread({
                    cb(null, e)
                })
            }
        }, true)
    }
}