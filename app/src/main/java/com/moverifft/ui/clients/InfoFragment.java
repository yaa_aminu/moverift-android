package com.moverifft.ui.clients;

import android.graphics.Color;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.moverifft.MoveRifft;
import com.moverifft.R;
import com.moverifft.truck.data.Truck;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.utils.GenericUtils;
import com.moverifft.utils.LocationUtils;
import com.moverifft.utils.PLog;

import javax.inject.Inject;

import butterknife.BindView;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yaaminu on 8/24/17.
 */

public abstract class InfoFragment extends BaseFragment {

    private static final String TAG = "InfoFragment";

    protected Trip trip;
    public static final String ARG_TRIP = "trip";

    @BindView(R.id.tv_current_location_name)
    TextView tvCurrentLocation;
    @BindView(R.id.tv_distance_travelled)
    TextView tvDistanceCovered;
    @BindView(R.id.tv_trip_name)
    TextView tv_trip_name;
    @Inject
    TripsApi tripsApi;

    void displayDriverInfo(Truck truck) {
        GenericUtils.assertThat(truck.getCurrentLocation() != null);
        LatLang currentTruckLocation = truck.getCurrentLocation();
        double distanceTraveled = currentTruckLocation.distanceInMetersTo(trip.getStartLocation());
        double totalDistance = trip.getStartLocation().distanceInMetersTo(trip.getEndLocation());
        tvDistanceCovered.setText(getString(R.string.distance_covered, normaliseDistance(distanceTraveled),
                normaliseDistance(totalDistance)));
        tripsApi.getLocationName(truck.getCurrentLocation())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSuccess, onError);
    }

    Action1<Address> onSuccess = new Action1<Address>() {
        public void call(Address address) {
            if (getActivity() != null) {
                tvCurrentLocation.setText(LocationUtils.summariseAddress(address));
            }
        }
    };

    Action1<Throwable> onError = new Action1<Throwable>() {
        public void call(Throwable throwable) {
            if (getActivity() != null) {
                PLog.e(TAG, throwable.getMessage(), throwable);
                // TODO: 10/8/17 show error
                tvCurrentLocation.setText(R.string.failed_to_retrieve_location);
                tvCurrentLocation.setError("");
                tvCurrentLocation.setTextColor(Color.RED);
            }
        }
    };


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_trip_name.setText(R.string.trip_info);
        tvDistanceCovered.setText(R.string.loading);
        tvCurrentLocation.setText(R.string.loading);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MoveRifft.depGraph.inject(this);
        trip = getArguments().getParcelable(ARG_TRIP);
        GenericUtils.assertThat(trip != null);
    }

    public void updateDistance(double distanceTraveled) {
        double totalDistance = trip.getStartLocation().distanceInMetersTo(trip.getEndLocation());
        tvDistanceCovered.setText(getString(R.string.distance_covered, normaliseDistance(distanceTraveled),
                normaliseDistance(totalDistance)));

    }


    private String normaliseDistance(double distance) {
        if (distance > 1000) {
            return GenericUtils.formatDistance(distance / 1000) + "Km";
        }
        return GenericUtils.formatDistance(distance) + "M";
    }
}
