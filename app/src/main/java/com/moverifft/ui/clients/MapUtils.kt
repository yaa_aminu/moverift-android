package com.moverifft.ui.clients

import android.content.Context
import android.graphics.Bitmap
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.moverifft.R
import com.moverifft.utils.GenericUtils.vectorToDrawable
import java.util.*

/**
 * Created by yaaminu on 10/17/17.
 */

object MapUtils {
    fun drawTrail(context: Context, googleMap: GoogleMap, currentLocation: LatLang?, trip: Trip, trails: List<TripTrail>) {
        googleMap.clear()
        plot(googleMap, trip.startLocation, vectorToDrawable(context, R.drawable.ic_lens_green_24dp))
        plot(googleMap, trip.endLocation, vectorToDrawable(context, R.drawable.ic_lens_red_24dp))
        if (currentLocation != null) {
            plot(googleMap, currentLocation, vectorToDrawable(context, R.drawable.ic_directions_car_black_24dp))
        }


        val polylineOptions = PolylineOptions()
                .width(25f)
                .color(ContextCompat.getColor(context, R.color.polyline_color))

        val points = ArrayList<LatLng>(trails.size + 1)
        points.add(trip.startLocation.toLatLng())
        trails.mapTo(points) { it.location.toLatLng() }
        polylineOptions.addAll(points)
        googleMap.addPolyline(polylineOptions)
        val cameraPosition = CameraPosition.Builder()
                .target(LatLang.centroid(trip.startLocation, trip.endLocation))
                .zoom(10f).tilt(20f)
                .bearing(0f)
                .build()
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun plot(googleMap: GoogleMap, latLang: LatLang, icon: Bitmap?) {
        val markerOptions = MarkerOptions()
                .position(latLang.toLatLng())
                .icon(BitmapDescriptorFactory.fromBitmap(icon))
        googleMap.addMarker(markerOptions)
    }

    fun drawLine(context: Context, googleMap: GoogleMap,
                 tripStatsAndRoutes: TripStatsAndRoutes) {

        val polylineOptions = PolylineOptions()
                .width(25f)
                .color(ContextCompat.getColor(context, R.color.polyline_color))

        polylineOptions.addAll(tripStatsAndRoutes.route)
        googleMap.addPolyline(polylineOptions)
        val latLngBounds = LatLngBounds.Builder()
        for (bound in tripStatsAndRoutes.bounds) {
            latLngBounds
                    .include(bound)
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 200))
    }
}
