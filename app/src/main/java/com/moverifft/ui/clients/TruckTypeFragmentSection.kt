package com.moverifft.ui.clients

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.ui.common.RecyclerViewBaseAdapter
import com.moverifft.users.data.TruckType
import com.moverifft.users.data.TruckTypeAdapter
import kotlinx.android.synthetic.main.truck_type_fragment_section.*

/**
 * Created by yaaminu on 1/11/18.
 */

class TruckTypeFragmentSection : BaseFragment() {
    companion object {
        const val ARG_SECTION = "arg_section"
    }

    lateinit var truckTypes: MutableList<TruckType>
    lateinit var onTruckTypeSelectedCb: OnTruckTypeSelected

    override fun getLayout(): Int {
        return R.layout.truck_type_fragment_section
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        onTruckTypeSelectedCb = parentFragment as OnTruckTypeSelected
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val section = arguments!!.getInt(ARG_SECTION)
        val (titlesRes, drawableRes) = getTruckTypes(section)

        val titles = resources.getStringArray(titlesRes)
        truckTypes = ArrayList()
        for (i in titles.indices) {
            truckTypes.add(TruckType(titles[i], (section * 20) + 1, drawableRes))
        }
    }

    private fun getTruckTypes(section: Int): Array<Int> {
        return when (section) {
            0 -> arrayOf(R.array.truck_type_light_duty, R.drawable.truck_1)
            1 -> arrayOf(R.array.truck_type_long_haul, R.drawable.truck_1)
            2 -> arrayOf(R.array.truck_type_project_haul, R.drawable.truck_1)
            else -> throw AssertionError("unknown truck category")
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        truck_type_recycler_view.layoutManager = GridLayoutManager(context, 2)
        truck_type_recycler_view.adapter = TruckTypeAdapter(delegate)
    }

    private val delegate = object : RecyclerViewBaseAdapter.Delegate<TruckType> {
        override fun onItemClick(adapter: RecyclerViewBaseAdapter<TruckType, *>?, view: View?, position: Int, id: Long) {
            confirmAndComplete(adapter!!.getItem(position))
        }

        override fun onItemLongClick(adapter: RecyclerViewBaseAdapter<TruckType, *>?, view: View?, position: Int, id: Long): Boolean {
            return false
        }

        override fun dataSet(): MutableList<TruckType> {
            return truckTypes
        }

        override fun context(): Context {
            return context
        }

    }

    fun confirmAndComplete(truckType: TruckType) {
        AlertDialog.Builder(context)
                .setMessage(getString(R.string.confirm_truck_type, truckType.title))
                .setPositiveButton(android.R.string.ok, { _: DialogInterface, _: Int ->
                    onTruckTypeSelectedCb.onTruckTypeSelected(truckType.truckId)
                }).setNegativeButton(android.R.string.cancel, null)
                .create()
                .show()
    }

    interface OnTruckTypeSelected {
        fun onTruckTypeSelected(type: Int)
    }
}