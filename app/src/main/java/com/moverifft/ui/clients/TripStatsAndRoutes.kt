package com.moverifft.ui.clients

import com.google.android.gms.maps.model.LatLng

/**
 * Created by yaaminu on 1/10/18.
 */

data class TripStatsAndRoutes(val distance: Double, val duration: Long,
                              var route: List<LatLng>, val bounds: List<LatLng>)