package com.moverifft.ui.clients

import android.os.Bundle
import android.view.View
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.utils.GenericUtils
import kotlinx.android.synthetic.main.fragment_order_details.*

class OrderDetailsFragment : BaseFragment() {

    lateinit var order: Order

    override fun getLayout(): Int {
        return R.layout.fragment_order_details
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        order = (activity as OrderProvider).getOrder()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_pick_up_location.text = order.pickup!!.name
        tv_destination.text = order.destination!!.name
        tv_truck_type_name.text = GenericUtils.getTruckTypeName(context, order.truckType)
        tv_estimated_charges.text = getString(R.string.estimated_charge, GenericUtils.FORMAT.format(
                order.estimatedCost))

        tv_goods_type.text = order.goods
        tv_note.text = order.notes
        payment_mode.text = getString(if (order.paymentMode == PAY_ON_PICKUP) R.string.pay_on_pickup else R.string.pay_on_delivery)
    }

    interface OrderProvider {
        fun getOrder(): Order
    }
}
