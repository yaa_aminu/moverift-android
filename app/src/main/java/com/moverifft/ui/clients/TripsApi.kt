package com.moverifft.ui.clients

import android.location.Address

import rx.Observable

/**
 * Created by yaaminu on 8/22/17.
 */

interface TripsApi {

    fun getCurrentTrip(): Observable<Trip>

    fun getTripCount(): Observable<Int>

    fun trips(truckId: String): Observable<List<Trip>>

    fun findTripById(tripId: String): Observable<Trip>

    fun tripsForCurrentUser(): Observable<List<Trip>>

    fun startTrip(hiredBy: String,
                  initialTruckLocation: LatLang,
                  startLocation: LatLang,
                  endLocation: LatLang, goodsType: String): Observable<Trip>

    fun endTrip(trip: Trip): Observable<Trip>

    fun initiateTrip(tripRequest: TripRequest): Observable<Any>

    fun rejectTripRequest(userId: String, goodsType: String, endLocation: LatLang): Observable<*>

    fun getLocationName(geoCoordinate: LatLang): Observable<Address>

    fun trail(tripId: String): Observable<List<TripTrail>>
    fun loadOrders(): Observable<List<Order>>
    fun getRoutes(from: LatLang, to: LatLang): Observable<TripStatsAndRoutes>

    fun bookTrip(order: Order): Observable<Order>
}
