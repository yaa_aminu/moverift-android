package com.moverifft.ui.clients

import android.os.Bundle
import com.moverifft.ui.common.BaseFragment

/**
 * Created by yaaminu on 1/7/18.
 */


abstract class WizardFragment : BaseFragment() {
    lateinit var listener: WizardFragmentListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listener = parentFragment as WizardFragmentListener
    }

    interface WizardFragmentListener {
        fun next(wizardFragment: WizardFragment)
        fun request(): TripRequest
    }
}