package com.moverifft.ui.clients

import android.os.Bundle
import android.view.View
import com.moverifft.R
import com.moverifft.utils.GenericUtils
import kotlinx.android.synthetic.main.fragment_confirm_booking.*

/**
 * Created by yaaminu on 1/16/18.
 */


class ConfirmBookingFragment : WizardFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_confirm_booking
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_pick_up_location.text = listener.request().startLocation!!.name
        tv_destination.text = listener.request().endLocation!!.name
        tv_truck_type_name.text = GenericUtils.getTruckTypeName(context, listener.request().truck!!.truckTypeIdentifier)
        tv_estimated_charges.text = getString(R.string.estimated_charge, GenericUtils.FORMAT.format(
                listener.request().estimatedCost
        ))
        tv_goods_type.text = listener.request().goodsType
        tv_note.text = listener.request().notes

        book.setOnClickListener {
            GenericUtils.showComfirmationDialog(context, getString(R.string.confirm_order_dialog_message)) {
                listener.next(this)
            }
        }
        pay_on_pickup.tag = ""
        pay_on_delivery.setOnClickListener(changePaymentMode)
        pay_on_pickup.setOnClickListener(changePaymentMode)
    }

    private val changePaymentMode: (View) -> Unit = {
        if (pay_on_pickup.tag != null) {
            pay_on_pickup.tag = null
            pay_on_delivery.tag = ""
            pay_on_delivery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp, 0)
            pay_on_pickup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_black_24dp, 0)
        } else {
            pay_on_delivery.tag = null
            pay_on_pickup.tag = ""
            pay_on_delivery.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_black_24dp, 0)
            pay_on_pickup.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp, 0)
        }
    }
}