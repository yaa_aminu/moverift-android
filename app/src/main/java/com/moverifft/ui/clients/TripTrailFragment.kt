package com.moverifft.ui.clients

import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.moverifft.R
import com.moverifft.truck.data.Truck
import com.moverifft.ui.common.BaseFragment
import com.moverifft.utils.GenericUtils
import com.moverifft.utils.ViewUtils
import kotlinx.android.synthetic.main.fragment_trip_trail.*

/**
 * Created by yaaminu on 10/10/17.
 */

class TripTrailFragment : BaseFragment(), OnMapReadyCallback {

    private var googleMap: GoogleMap? = null
    private var route: TripStatsAndRoutes? = null

    private var trip: Trip? = null
    private var truck: Truck? = null
    private var tripTrails = emptyList<TripTrail>()

    override fun getLayout(): Int {
        return R.layout.fragment_trip_trail
    }


    fun setData(trip: Trip, tripTrails: List<TripTrail>, truck: Truck) {
        this.trip = trip
        this.truck = truck
        this.tripTrails = tripTrails
        refreshDisplay()
        doDraw(this.route)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addGoogleMap()
    }

    private fun addGoogleMap() {
        val supportMapFragment = SupportMapFragment.newInstance(GoogleMapOptions()
                .mapType(GoogleMap.MAP_TYPE_NORMAL))

        supportMapFragment.getMapAsync(this)

        fragmentManager
                .beginTransaction()
                .replace(R.id.trip_trail_map_fragment, supportMapFragment)
                .commit()
    }

    @Suppress("DEPRECATION")
    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        view?.viewTreeObserver
                ?.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        if (view != null) {
                            view!!.viewTreeObserver.removeGlobalOnLayoutListener(this)
                            refreshDisplay()
                            doDraw(this@TripTrailFragment.route)
                        }
                    }
                })
    }

    private fun refreshDisplay() {
        if (trip != null && truck != null && googleMap != null) {
            MapUtils.drawTrail(context, googleMap!!, truck!!.currentLocation, trip!!, tripTrails)
            if (tripTrails.isEmpty()) {
                GenericUtils.fitMapToBounds(googleMap!!, 120, trip!!.startLocation, trip!!.endLocation)
            } else {
                GenericUtils.fitMapToBounds(googleMap!!, 120, tripTrails[0].location,
                        trip!!.startLocation,
                        trip!!.endLocation)
            }
        }
    }

    fun clearAndShowProgress() {
        if (googleMap != null) {
            googleMap!!.clear()
        }
        ViewUtils.showViews(progress)
    }

    fun hideProgress() {
        ViewUtils.hideViews(progress)
    }

    fun drawRoute(tripStatsAndRoutes: TripStatsAndRoutes) {
        this.route = tripStatsAndRoutes
        doDraw(this.route)
    }

    private fun doDraw(tripStatsAndRoutes: TripStatsAndRoutes?) {
        if (this.googleMap != null && tripStatsAndRoutes != null) {
            MapUtils.drawLine(context, googleMap!!, tripStatsAndRoutes)
        }
    }
}
