package com.moverifft.ui.clients;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.Pair;

import com.moverifft.BuildConfig;
import com.moverifft.MoveRifft;
import com.moverifft.R;
import com.moverifft.truck.data.Truck;
import com.moverifft.truck.data.TruckApi;
import com.moverifft.ui.driver.BaseActivity;
import com.moverifft.ui.driver.TripInfoFragmentDriver;
import com.moverifft.utils.PLog;
import com.moverifft.utils.UiHelpers;

import java.util.List;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripDetailsActivity extends BaseActivity implements LocationListener {

    public static final String EXTRA_TRIP = "trip";
    private static final String TAG = "TripDetailsActivity";

    @Inject
    TruckApi truckApi;
    @Inject
    public TripsApi tripsApi;

    private InfoFragment infoFragment;
    private TripTrailFragment tripTrailFragment;

    private LocationManager locationManager;
    private Location ourLocation;
    @Nullable
    private Trip trip;
    @Nullable
    private ProgressDialog progressDialog;
    @Nullable
    private Truck truck;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MoveRifft.depGraph.inject(this);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        setContentView(R.layout.fragment_trip_details);
        trip = getIntent().getParcelableExtra(EXTRA_TRIP);
        if (trip != null) {
            initFragments(trip);
            loadAndShowTrail(trip);
        } else {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
            tripsApi.findTripById(getIntent().getStringExtra(EXTRA_TRIP))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Trip>() {
                        @Override
                        public void call(Trip trip) {
                            TripDetailsActivity.this.trip = trip;
                            initFragments(trip);
                            loadAndShowTrail(trip);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            UiHelpers.showErrorDialog(TripDetailsActivity.this, throwable.getMessage());
                        }
                    });
        }
    }

    private void initFragments(@NonNull Trip trip) {
        if (BuildConfig.FLAVOR.equals(BuildConfig.FLAVOR_NAME_DRIVER)) {
            infoFragment = TripInfoFragmentDriver.create(trip);
        } else {
            infoFragment = TripInfoFragmentClient.create(trip);
        }
        tripTrailFragment = new TripTrailFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.trip_info, infoFragment)
                .replace(R.id.two_map_container, tripTrailFragment)
                .commit();
        final ProgressDialog dialog = UiHelpers.showProgressDialog(this);
        tripsApi.getRoutes(trip.getStartLocation(), trip.getEndLocation())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<TripStatsAndRoutes>() {
                    @Override
                    public void call(TripStatsAndRoutes tripStatsAndRoutes) {
                        dialog.dismiss();
                        tripTrailFragment.drawRoute(tripStatsAndRoutes);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        dialog.dismiss();
                    }
                });
    }


    @Override
    @SuppressLint("NewApi")
    //offending code(the permission check) will never be executed on old apis
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
            return;
        }
        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, Looper.myLooper());
    }


    @Override
    protected void onPause() {
        locationManager.removeUpdates(this);
        super.onPause();
    }

    @Override
    public void onLocationChanged(Location location) {
        ourLocation = location;
        updateLocation(location);
    }

    private void updateLocation(Location location) {
        if (trip != null && truck != null) {
            final LatLang currentTruckLocation = truck.getCurrentLocation();
            if (currentTruckLocation != null) {
                infoFragment.updateDistance(trip.getStartLocation()
                        .distanceInMetersTo(LatLang.from(location)));
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Nullable
    public LatLang getLocationNow() {
        if (ourLocation == null) return null;
        return LatLang.from(ourLocation);
    }

    private void loadAndShowTrail(final Trip trip) {
        tripTrailFragment.clearAndShowProgress();
        truckApi
                .findTruckById(trip.getTruckId())
                .zipWith(tripsApi.trail(trip.getTripId()), new Func2<Truck, List<TripTrail>, Pair<Truck, List<TripTrail>>>() {
                    @Override
                    public Pair<Truck, List<TripTrail>> call(Truck truck, List<TripTrail> tripTrails) {
                        return Pair.create(truck, tripTrails);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Pair<Truck, List<TripTrail>>>() {
                    @Override
                    public void call(Pair<Truck, List<TripTrail>> ret) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        TripDetailsActivity.this.truck = ret.first;
                        tripTrailFragment
                                .setData(trip, ret.second, ret.first);
                        tripTrailFragment.hideProgress();
                        infoFragment.displayDriverInfo(ret.first);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        throwable.printStackTrace();
                        PLog.d(TAG, throwable.getMessage(), throwable);
                        UiHelpers.showErrorDialog(TripDetailsActivity.this, throwable.getMessage());
                    }
                });
    }
}
