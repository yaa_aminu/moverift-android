package com.moverifft.ui.clients

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.moverifft.R
import com.moverifft.utils.GenericUtils
import org.jetbrains.anko.bundleOf

class PagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    private val fragments = mutableMapOf<String, TruckTypeFragmentSection>()
    override fun getCount(): Int {
        return 3
    }

    override fun getItem(position: Int): Fragment {
        var fragment = fragments[position.toString()]
        if (fragment == null) {
            fragment = TruckTypeFragmentSection()
                    .apply {
                        arguments = bundleOf(TruckTypeFragmentSection.ARG_SECTION to position)
                    }
            fragments[position.toString()] = fragment
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> GenericUtils.getString(R.string.light_duty_trucks)
            1 -> GenericUtils.getString(R.string.long_haul_trucks)
            2 -> GenericUtils.getString(R.string.project_haul)
            else -> throw AssertionError("unknown section")
        }
    }

}