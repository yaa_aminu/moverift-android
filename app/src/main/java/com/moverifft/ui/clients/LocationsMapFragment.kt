package com.moverifft.ui.clients

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.utils.GenericUtils
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by yaaminu on 8/24/17.
 */

class LocationsMapFragment : BaseFragment(), OnMapReadyCallback {

    private var googleMap: GoogleMap? = null

    private var locations: List<LabeledLocation>? = null

    private var route: TripStatsAndRoutes? = null

    private var markers: MutableMap<String, Marker>? = null


    override fun getLayout(): Int {
        return R.layout.fragment_two_map_fragment
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val supportMapFragment = SupportMapFragment.newInstance(GoogleMapOptions()
                .mapType(GoogleMap.MAP_TYPE_NORMAL))
        locations = arguments.getParcelableArrayList(ARG_LOCATIONS)
        markers = if (locations != null) {
            HashMap(locations!!.size)
        } else {
            HashMap(2)
        }
        supportMapFragment.getMapAsync(this)

        childFragmentManager
                .beginTransaction()
                .replace(R.id.two_map_container, supportMapFragment)
                .commit()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        if (view != null) {
            Handler()
                    .postDelayed({
                        if (activity != null) {
                            updateLocations()
                            doDraw(route)
                        }
                    }, TimeUnit.SECONDS.toMillis(1))
        }
    }

    fun drawRoute(tripStatsAndRoutes: TripStatsAndRoutes) {
        this.route = tripStatsAndRoutes
        doDraw(this.route)
    }

    private fun doDraw(tripStatsAndRoutes: TripStatsAndRoutes?) {
        if (this.googleMap != null && tripStatsAndRoutes != null) {
            MapUtils.drawLine(context, googleMap!!, tripStatsAndRoutes)
        }
    }

    private fun updateLocations() {
        if (this.googleMap != null && locations != null && !locations!!.isEmpty()) {
            for (location in locations!!) {
                val marker = markers!![location.tag]
                if (marker != null) {
                    marker.setPosition(LatLng(location.lat, location.lng))
                } else {
                    val markerOptions = MarkerOptions()
                            .position(LatLng(location.lat, location.lng))
                            .title(location.label)
                    if (location.marker != null) {
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(location.marker))
                    }
                    markers!!.put(location.tag, this.googleMap!!
                            .addMarker(markerOptions))
                }
            }
            if (locations!!.size == 1) {
                val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(locations!![0].lat, locations!![0].lng))
                        .zoom(14f).tilt(20f)
                        .bearing(0f)
                        .build()

                this.googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            } else {
                GenericUtils.fitMapToBounds(googleMap!!, 140, locations)
            }
        }
    }

    companion object {

        val ARG_LOCATIONS = "locations"


        @JvmStatic
        fun create(locationsFragmentParams: LocationsFragmentParams): LocationsMapFragment {
            val bundle = Bundle(1)
            val locations = ArrayList<LabeledLocation>(3)

            locations.apply {
                add(locationsFragmentParams.from)
                add(locationsFragmentParams.to)
                add(locationsFragmentParams.truckLocation)
            }
            bundle.putParcelableArrayList(ARG_LOCATIONS, locations)
            val twoLocationsMapFragment = LocationsMapFragment()
            twoLocationsMapFragment.arguments = bundle
            return twoLocationsMapFragment
        }
    }

    data class LocationsFragmentParams(val from: LabeledLocation, val to: LabeledLocation, val truckLocation: LabeledLocation)

}
