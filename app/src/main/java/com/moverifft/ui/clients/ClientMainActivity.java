package com.moverifft.ui.clients;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.moverifft.MoveRifft;
import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.ui.driver.BaseActivity;
import com.moverifft.ui.driver.LocationListener;
import com.moverifft.users.data.UserApi;
import com.moverifft.utils.AboutActivity;
import com.moverifft.utils.GenericUtils;
import com.moverifft.utils.PLog;
import com.moverifft.utils.PaymentInstruction;
import com.moverifft.utils.SettingsActivity;
import com.rey.slidelayout.SlideLayout;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

/**
 * Created by yaaminu on 8/17/17.
 */

public class ClientMainActivity extends BaseActivity implements
        android.location.LocationListener, View.OnClickListener, SlideLayout.OnStateChangedListener {

    public static final String NO_LOCATION = "no_location";
    private final Set<LocationListener> listeners = new HashSet<>(3);
    private LocationManager locationManager;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 3001;

    @BindView(R.id.left_menu_list)
    ListView leftMenuList;

    @BindView(R.id.slide_layout)
    SlideLayout slideLayout;
    @BindView(R.id.tv_user_name)
    TextView tv_user_name;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    UserApi userApi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MoveRifft.depGraph.inject(this);
        setContentView(R.layout.activity_client);

        ButterKnife.bind(this);
        locationManager = ((LocationManager) getSystemService(LOCATION_SERVICE));

        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setNavigationOnClickListener(this);
        toolbar.setTitle(R.string.app_name);


        final String[] sideMenuItems = getResources().getStringArray(R.array.left_menu_items);
        @DrawableRes int[] icons = new int[5];
        icons[0] = R.drawable.ic_my_trips_24dp;
        icons[1] = R.drawable.ic_notifications_black_24dp;
        icons[2] = R.drawable.ic_payment;
        icons[3] = R.drawable.ic_settings_black_24dp;
        icons[4] = R.drawable.ic_info_black_24dp;
        leftMenuList.setAdapter(new SideMenuAdapter(sideMenuItems, icons));


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new BookTruckFragment())
                .commit();

        //noinspection ConstantConditions //if we are here, currentUser cannot be null
        tv_user_name.setText(userApi.getCurrentUser().getName());
        String dp = userApi.getCurrentUser().getDp();
        GenericUtils.loadDp(dp, (ImageView) findViewById(R.id.user_avatar));

        slideLayout.setOnStateChangedListener(this);
    }

    @OnItemClick(R.id.left_menu_list)
    public void onMenuItemClick(int position) {
        slideLayout.closeLeftMenu(true);
        if (position == 0) {
            startActivity(new Intent(this, TripsActivity.class));
        } else if (position == 1) {
            startActivity(new Intent(this, NotificationActivity.class));
        } else if (position == 2) {
            startActivity(new Intent(this, PaymentInstruction.class));
        } else if (position == 3) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (position == 4) {
            startActivity(new Intent(this, AboutActivity.class));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //always true
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            }
            return;
        }
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, new NoLocationFragment(), NO_LOCATION)
                    .commit();
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(NO_LOCATION);
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new BookTruckFragment())
                        .commit();
            }
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }


    @Override
    public void onLocationChanged(Location location) {
        for (com.moverifft.ui.driver.LocationListener listener : listeners) {
            listener.onLocation(location);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {
        if (slideLayout.getState() == 0) {
            slideLayout.openLeftMenu(true);
        } else {
            slideLayout.closeLeftMenu(true);
        }
    }

    private static final String TAG = "ClientMainActivity";

    @Override
    public void onStateChanged(View v, int old_state, int new_state) {
        PLog.d(TAG, "old state " + old_state + ", newState: " + new_state);
        if (new_state != 0 && v != slideLayout.getContentView()) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        }
    }

    @Override
    public void onOffsetChanged(View v, float offsetX, float offsetY, int state) {

    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof BaseFragment && ((BaseFragment) currentFragment).onBackPressed()) {
            return;
        }

        if (slideLayout.getState() != 0) {
            slideLayout.closeLeftMenu(true);
            return;
        }
        super.onBackPressed();
    }
}

