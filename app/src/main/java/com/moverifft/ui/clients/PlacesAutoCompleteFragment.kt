package com.moverifft.ui.clients

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.ui.common.RecyclerViewBaseAdapter
import com.moverifft.utils.PLog
import com.moverifft.utils.UiHelpers
import com.moverifft.utils.ViewUtils
import kotlinx.android.synthetic.main.fragment_places_auto_complete.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by yaaminu on 1/8/18.
 */
//5.5905595,-0.2502697
private const val STATE_LOADING = 1
private const val STATE_ERROR = 2
private const val STATE_NOT_LOADING = 3
private const val STATE_LOADED = 4

class PlacesAutoCompleteFragment : BaseFragment() {
    private lateinit var onPlaceSelectedCallback: OnPlaceSelectedCallback

    private var places = emptyList<LatLang>()
    private lateinit var placesAdapter: PlacesAdapter
    private var state = STATE_NOT_LOADING

    override fun getLayout(): Int {
        return R.layout.fragment_places_auto_complete
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onPlaceSelectedCallback = parentFragment as OnPlaceSelectedCallback
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        places_recycler_view.layoutManager = LinearLayoutManager(context)
        placesAdapter = PlacesAdapter(delegate)
        places_recycler_view.adapter = placesAdapter
        places_recycler_view.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
    }

    fun search(str: String, currentLocation: LatLang?) {
        if (str.isBlank() && currentLocation == null) {
            places = emptyList()
            placesAdapter.notifyDataChanged()
        } else {
            PLog.d(TAG, "searching for $str; $currentLocation")
            state = STATE_LOADING
            placesAdapter.notifyDataChanged()
            PlacesManager.searchPlace(str.toLowerCase(), currentLocation)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (!isDestroyed) {
                            state = STATE_LOADED
                            places = it
                            placesAdapter.notifyDataChanged()
                            if (it.isEmpty()) {
                                onPlaceSelectedCallback.onNothingRetrieved()
                            }
                        }
                    }, {
                        if (!isDestroyed) {
                            places = emptyList()
                            state = STATE_ERROR
                            placesAdapter.notifyDataChanged()
                        }
                    })
        }
    }


    private val delegate = object : RecyclerViewBaseAdapter.Delegate<LatLang> {
        override fun onItemClick(adapter: RecyclerViewBaseAdapter<LatLang, *>?, view: View?, position: Int, id: Long) {
            val latLang = adapter!!.getItem(position)
            places = emptyList()
            adapter.notifyDataChanged()
            if (latLang.latitude == 0.0 && latLang.longitude == 0.0) {
                val pDialog = UiHelpers.showProgressDialog(context)
                PlacesManager.fetchPlaceDetails(latLang, { loadedLocation: LatLang?, exception: Exception? ->
                    if (!isDestroyed) {
                        pDialog.dismiss()
                        if (exception == null) {
                            onPlaceSelectedCallback.onPlaceSelected(loadedLocation!!)
                        } else {
                            UiHelpers.showErrorDialog(context, exception.message)
                        }
                    }
                })
            } else {
                onPlaceSelectedCallback.onPlaceSelected(latLang)
            }
        }

        override fun onItemLongClick(adapter: RecyclerViewBaseAdapter<LatLang, *>?, view: View?, position: Int, id: Long): Boolean {
            return false
        }

        override fun dataSet(): List<LatLang> {
            ViewUtils.showByFlag(!places.isEmpty(), places_recycler_view)
            ViewUtils.showByFlag(state == STATE_LOADING && places.isEmpty(), progress)
            ViewUtils.showByFlag((state == STATE_LOADED || state == STATE_NOT_LOADING || state == STATE_ERROR) && places.isEmpty(), empty_text)
            empty_text.text = when (state) {
                STATE_LOADED -> getString(R.string.suggestion_description)
                STATE_NOT_LOADING -> getString(R.string.suggestion_description)
                STATE_ERROR -> getString(R.string.trouble_auto_completing_address)
                STATE_LOADING -> getString(R.string.suggestion_description)
                else -> throw AssertionError()
            }
            return places
        }

        override fun context(): Context {
            return context
        }
    }

    interface OnPlaceSelectedCallback {
        fun onPlaceSelected(latLang: LatLang)
        fun onNothingRetrieved()
    }

}
