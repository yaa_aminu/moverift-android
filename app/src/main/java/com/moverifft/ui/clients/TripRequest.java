package com.moverifft.ui.clients;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.moverifft.BuildConfig;
import com.moverifft.truck.data.Truck;

/**
 * Created by yaaminu on 9/19/17.
 */

public class TripRequest implements Parcelable {
    private Truck truck;
    private String clientId;
    private LatLang driverLocation;
    private LatLang clientLocation;
    private LatLang startLocation;
    private LatLang endLocation;
    private int acceptedFlags; //0x1 - driver accepted, 0x2-client accepted, 0x3-both accepted
    private String goodsType;
    private String notes;
    private long estimatedCost;

    public TripRequest() {
    }

    public TripRequest(Truck truck, String clientId,
                       LatLang driverLocation,
                       LatLang clientLocation,
                       LatLang startLocation,
                       LatLang endLocation,
                       int acceptedFlags,
                       String goodsType) {
        this.truck = truck;
        this.clientId = clientId;
        this.driverLocation = driverLocation;
        this.clientLocation = clientLocation;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.acceptedFlags = acceptedFlags;
        this.goodsType = goodsType;
    }

    public void setStartLocation(@Nullable LatLang startLocation) {
        this.startLocation = startLocation;
    }

    public void setEndLocation(@Nullable LatLang endLocation) {
        this.endLocation = endLocation;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public void setGoodsType(String goodsType) {
        this.goodsType = goodsType;
    }

    protected TripRequest(Parcel in) {
        truck = in.readParcelable(Truck.class.getClassLoader());
        clientId = in.readString();
        final ClassLoader latLangClassLoader = LatLang.class.getClassLoader();
        driverLocation = in.readParcelable(latLangClassLoader);
        clientLocation = in.readParcelable(latLangClassLoader);
        startLocation = in.readParcelable(latLangClassLoader);
        endLocation = in.readParcelable(latLangClassLoader);
        acceptedFlags = in.readInt();
        goodsType = in.readString();
    }

    public static final Creator<TripRequest> CREATOR = new Creator<TripRequest>() {
        @Override
        public TripRequest createFromParcel(Parcel in) {
            return new TripRequest(in);
        }

        @Override
        public TripRequest[] newArray(int size) {
            return new TripRequest[size];
        }
    };

    public void accept() {
        acceptedFlags |= (BuildConfig.FLAVOR.equals(BuildConfig.FLAVOR_NAME_CLIENT) ? 0x2 : 0x1);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(truck, flags);
        dest.writeString(clientId);
        dest.writeParcelable(driverLocation, flags);
        dest.writeParcelable(clientLocation, flags);
        dest.writeParcelable(startLocation, flags);
        dest.writeParcelable(endLocation, flags);
        dest.writeInt(acceptedFlags);
        dest.writeString(goodsType);
    }

    @Override
    public String toString() {
        return "TripRequestDriver{" +
                "truck=" + truck +
                ", clientId='" + clientId + '\'' +
                ", driverLocation=" + driverLocation +
                ", clientLocation=" + clientLocation +
                ", startLocation=" + startLocation +
                ", endLocation=" + endLocation +
                ", acceptedFlags=" + acceptedFlags +
                ", goods type=" + goodsType +
                '}';
    }

    @Nullable
    public Truck getTruck() {
        return truck;
    }

    @Nullable
    public LatLang getCurrentLocation() {
        return clientLocation;
    }

    public void setCurrenLocation(LatLang location) {
        this.clientLocation = location;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Nullable
    public LatLang getTruckLocation() {
        return driverLocation;
    }

    public void setTruckLocation(LatLang location) {
        this.driverLocation = location;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    @Nullable
    public LatLang getStartLocation() {
        return startLocation;
    }

    @Nullable
    public LatLang getEndLocation() {
        return endLocation;
    }

    public String getGoodsType() {
        return goodsType;
    }

    public String getClientId() {
        return clientId;
    }

    public long getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(long estimatedCost) {
        this.estimatedCost = estimatedCost;
    }
}
