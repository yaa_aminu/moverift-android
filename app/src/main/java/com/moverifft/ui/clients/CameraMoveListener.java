package com.moverifft.ui.clients;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.GoogleMap;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by yaaminu on 9/22/17.
 */

public class CameraMoveListener implements GoogleMap.OnCameraMoveListener {

    @NonNull
    private final Observable<LatLang> observable;
    @Nullable
    private Subscriber<? super LatLang> subscriber;
    private final GoogleMap googleMap;

    public CameraMoveListener(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        observable = Observable.create(new Observable.OnSubscribe<LatLang>() {
            @Override
            public void call(Subscriber<? super LatLang> subscriber) {
                CameraMoveListener.this.subscriber = subscriber;
            }
        });
    }

    @Override
    public void onCameraMove() {
        if (subscriber != null) {
            subscriber.onNext(LatLang.from(googleMap.getCameraPosition().target));
        }
    }

    public Observable<LatLang> observe() {
        return observable;
    }
}
