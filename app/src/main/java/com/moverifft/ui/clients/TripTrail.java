package com.moverifft.ui.clients;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by yaaminu on 10/17/17.
 */

public class TripTrail {
    public final String tripId;
    public final LatLang location;
    public final long timestamp;

    public TripTrail(@NonNull String tripId, long timestamp, @NonNull LatLang location) {
        this.tripId = tripId;
        this.timestamp = timestamp;
        this.location = location;
    }

    public static TripTrail fromJson(String tripId, String tripTrail) {
        try {
            JSONObject jsonObject = new JSONObject(tripTrail);
            return new TripTrail(tripId, jsonObject.getLong("timeReported"),
                    new LatLang(jsonObject.getDouble("lat"), jsonObject.getDouble("lng")));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public static class TripAndTrail {
        public final Trip trip;
        public final List<TripTrail> tripTrails;

        public TripAndTrail(Trip trip, List<TripTrail> tripTrails) {
            this.trip = trip;
            this.tripTrails = tripTrails;
        }
    }
}
