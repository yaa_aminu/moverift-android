package com.moverifft.ui.driver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.ui.common.NotificationEntriesAdapter;
import com.moverifft.ui.common.RecyclerViewBaseAdapter;
import com.moverifft.utils.PLog;
import com.moverifft.utils.ViewUtils;

import java.util.List;

import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by yaaminu on 8/18/17.
 */

public class NotificationsFragment extends BaseFragment {
    private static final String TAG = "NotificationsFragment";
    private NotificationEntriesAdapter adapter;

    @Override
    protected int getLayout() {
        return R.layout.fragment_notifications;
    }

    @BindView(R.id.notifications_recycler_view)
    RecyclerView notifications_recycler_view;
    @BindView(R.id.tv_empty)
    TextView emptyView;

    Realm realm;
    RealmResults<NotificationEntry> notificationEntries;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        notificationEntries = realm.where(NotificationEntry.class).findAllSortedAsync(NotificationEntry.Companion.getFIELD_TIMESTAMP());
        notificationEntries.addChangeListener(new RealmChangeListener<RealmResults<NotificationEntry>>() {
            @Override
            public void onChange(RealmResults<NotificationEntry> notificationEntries) {
                adapter.notifyDataChanged();
            }
        });
    }

    @Override
    public void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notifications_recycler_view.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new NotificationEntriesAdapter(delegate);
        notifications_recycler_view.setAdapter(adapter);
    }

    private final RecyclerViewBaseAdapter.Delegate<NotificationEntry> delegate = new RecyclerViewBaseAdapter.Delegate<NotificationEntry>() {
        @Override
        public void onItemClick(RecyclerViewBaseAdapter<NotificationEntry, ?> adapter, View view, int position, long id) {
            PLog.d(TAG, "clicked on " + adapter.getItem(position).getText());
            Intent intent = adapter.getItem(position).getAction(getContext());
            if (intent != null) {
                startActivity(intent);
            }
        }

        @Override
        public boolean onItemLongClick(RecyclerViewBaseAdapter<NotificationEntry, ?> adapter, View view, int position, long id) {
            return false;
        }

        @NonNull
        @Override
        public List<NotificationEntry> dataSet() {
            ViewUtils.showByFlag(notificationEntries.isEmpty(), emptyView);
            ViewUtils.showByFlag(!notificationEntries.isEmpty(), notifications_recycler_view);
            return notificationEntries;
        }

        @NonNull
        @Override
        public Context context() {
            return getContext();
        }
    };

}
