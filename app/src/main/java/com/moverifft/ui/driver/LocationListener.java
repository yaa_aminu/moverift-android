package com.moverifft.ui.driver;

import android.location.Location;
import android.support.annotation.Nullable;

/**
 * Created by yaaminu on 8/18/17.
 */
public interface LocationListener {
    void onLocation(@Nullable Location location);

}
