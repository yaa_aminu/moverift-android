package com.moverifft.ui.driver;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.users.data.UserManager;
import com.moverifft.utils.GenericUtils;
import com.squareup.picasso.Callback;

import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;

import static com.moverifft.BuildConfig.FLAVOR;
import static com.moverifft.BuildConfig.FLAVOR_NAME_CLIENT;
import static com.moverifft.utils.FileUtils.resolveContentUriToFilePath;
import static com.moverifft.utils.TaskManager.executeNow;
import static com.moverifft.utils.TaskManager.executeOnMainThread;
import static com.moverifft.utils.ThreadUtils.isMainThread;
import static com.moverifft.utils.UiHelpers.showErrorDialog;
import static com.moverifft.utils.ViewUtils.enableByFlag;
import static com.moverifft.utils.ViewUtils.enableViews;
import static com.squareup.picasso.Picasso.with;

/**
 * Created by yaaminu on 11/24/17.
 */

public class ChooseDpFragment extends BaseFragment {
    public static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 1002;
    @BindView(R.id.dp_preview)
    ImageView dp_preview;
    @BindView(R.id.continue_)
    Button continue_;
    @BindView(R.id.choose_dp)
    View chooseDp;
    private ProgressDialog progressDialog;

    @Override
    protected int getLayout() {
        return R.layout.fragment_choose_dp;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        enableByFlag(FLAVOR.equals(FLAVOR_NAME_CLIENT), continue_);
        ((TextView) view.findViewById(R.id.welcome_text)).setText(getString(R.string.welcome_template,
                UserManager.getInstance().getCurrentUser().getName()));
    }

    @OnClick({R.id.continue_, R.id.choose_dp, R.id.dp_preview})
    void chooseDp(View v) {
        if (v.getId() == R.id.continue_) {
            if (dpPath != null) {
                progressDialog.show();
                // TODO: 11/24/17 update current user dp
                UserManager.getInstance()
                        .updateCurrentUserDp(dpPath, callback);
            } else {
                confirmAndContinue();
            }
            return;
        }
        if (GenericUtils.hasPermission(getActivity(), true, WRITE_EXTERNAL_STORAGE_REQUEST_CODE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            chooseDp();
        }
    }

    private void confirmAndContinue() {
        new AlertDialog.Builder(getContext())
                .setMessage("Do you want to continue without profile picture?")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UserManager.getInstance()
                                .skipDp();
                        ((LoginActivity) getActivity())
                                .onUpdateDp();
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .create().show();
    }

    private final com.moverifft.ui.common.Callback<Boolean> callback = new com.moverifft.ui.common.Callback<Boolean>() {
        @Override
        public void done(Exception e, Boolean aBoolean) {
            progressDialog.dismiss();
            if (e != null) {
                showErrorDialog(getContext(), e.getMessage());
            } else {
                ((LoginActivity) getActivity())
                        .onUpdateDp();
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == WRITE_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (GenericUtils.wasPermissionGranted(permissions, grantResults)) {
                chooseDp();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void chooseDp() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, 1001);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                updatePreview(uri);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private volatile String dpPath = null;

    private void updatePreview(final Uri uri) {
        progressDialog.show();
        executeNow(new Runnable() {
            Exception e = null;

            @Override
            public void run() {
                if (isMainThread()) {
                    if (e != null) {
                        progressDialog.dismiss();
                        showErrorDialog(getContext(), e.getMessage());
                    } else {
                        loadImage(progressDialog, dpPath);
                    }
                } else {
                    String path = resolveContentUriToFilePath(uri, true);
                    if (path != null && new File(path).exists()) {
                        dpPath = path;
                        executeOnMainThread(this);
                    } else {
                        e = new Exception("Failed to load image.");
                        executeOnMainThread(this);
                    }
                }
            }
        }, false);
    }

    private void loadImage(final ProgressDialog progressDialog, String path) {
        with(getContext())
                .load(new File(path))
                .into(dp_preview, new Callback() {
                    @Override
                    public void onSuccess() {
                        enableViews(continue_);
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError() {
                        dpPath = null;
                        enableByFlag(FLAVOR.equals(FLAVOR_NAME_CLIENT), continue_);
                        progressDialog.dismiss();
                    }
                });
    }
}
