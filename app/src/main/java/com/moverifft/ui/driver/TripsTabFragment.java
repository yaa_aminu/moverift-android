package com.moverifft.ui.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.moverifft.R;
import com.moverifft.ui.clients.Trip;
import com.moverifft.ui.clients.TripDetailsActivity;
import com.moverifft.ui.common.BaseFragment;

/**
 * Created by yaaminu on 10/29/17.
 */

public class TripsTabFragment extends BaseFragment {

    @Override
    protected int getLayout() {
        return R.layout.fragment_trip_tab;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    void onTripSelected(final Trip trip) {
        Intent intent = new Intent(getContext(),
                TripDetailsActivity.class);
        intent.putExtra(TripDetailsActivity.EXTRA_TRIP, trip);
        startActivity(intent);
    }
}
