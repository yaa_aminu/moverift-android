@file:Suppress("DEPRECATION")

package com.moverifft.ui.driver


import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import butterknife.OnClick
import com.google.i18n.phonenumbers.NumberParseException
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.users.data.UserManager
import com.moverifft.utils.GenericUtils
import com.moverifft.utils.PhoneNumberNormaliser
import com.moverifft.utils.UiHelpers
import kotlinx.android.synthetic.main.fragment_login_client.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


/**
 * Created by yaaminu on 8/13/17.
 */

class LoginFragment : BaseFragment() {


    lateinit var dialog: ProgressDialog

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        GenericUtils.assertThat(context is LoginActivity, "can only be embeded in " + LoginActivity::class.java.name)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = ProgressDialog(context)
        dialog.setCancelable(false)
        dialog.setMessage("Logging in")
    }

    override fun getLayout(): Int {
        return R.layout.fragment_login_client
    }

    @OnClick(R.id.bt_login)
    internal fun login() {
        if (validate()) {
            dialog.show()

            val phoneNumber = et_phone.text.toString().trim()
            try {
                UserManager.getInstance()
                        .login(et_name!!.text.toString().trim(),
                                PhoneNumberNormaliser.toIEE(phoneNumber, "GH"),
                                et_email.text.toString().trim())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ user ->
                            dialog.dismiss()
                            (activity as LoginActivity)
                                    .onLoggedIn(user)
                        }) { throwable ->
                            dialog.dismiss()
                            GenericUtils.showDialog(context, throwable.message)
                        }
            } catch (e: NumberParseException) {
                UiHelpers.showErrorDialog(context, e.message)
            }

        }
    }

    private fun validate(): Boolean {
        if (et_name.text.toString().trim { it <= ' ' }.isEmpty()) {
            et_name.error = getString(R.string.name_required)
            return false
        }
        val phoneNumber = et_phone!!.text.toString().trim { it <= ' ' }
        if (phoneNumber.isEmpty()) {
            et_phone.error = getString(R.string.phone_required)
            return false
        }
        if (!/*Locale.getDefault().getCountry())*/PhoneNumberNormaliser.isValidPhoneNumber(phoneNumber, "GH")) {
            et_phone!!.error = "Phone number is invalid"
            return false
        }

        val email = et_email.text.trim()
        if (!GenericUtils.isValidEmail(email)) {
            et_email.error = getString(R.string.invalid_email)
            return false
        }
        return true
    }
}
