package com.moverifft.ui.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;

import butterknife.BindView;

/**
 * Created by yaaminu on 8/18/17.
 */

public class MyTripsFragment extends BaseFragment {
    @Override
    protected int getLayout() {
        return R.layout.fragment_my_trips;
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle("Trips");
    }
}
