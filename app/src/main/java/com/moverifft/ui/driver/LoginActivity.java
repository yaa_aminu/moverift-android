package com.moverifft.ui.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.moverifft.MoveRifft;
import com.moverifft.R;
import com.moverifft.truck.data.TruckApi;
import com.moverifft.ui.common.LauncherActivity;
import com.moverifft.users.data.User;
import com.moverifft.users.data.UserApi;
import com.moverifft.users.data.UserManager;

import javax.inject.Inject;

/**
 * Created by yaaminu on 8/13/17.
 */

public class LoginActivity extends BaseActivity {
    private static final String TAG = "LoginActivity";

    @Inject
    UserApi instance;
    @Inject
    TruckApi truckManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MoveRifft.depGraph.inject(this);
        if (UserManager.getInstance().isCurrentUserSetup()) {
            startActivity(new Intent(this, LauncherActivity.class));
            finish();
        } else {
            setContentView(R.layout.activity_login);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, new LoginIntroFragment())
                    .commit();
        }
    }

    public void onLoggedIn(User user) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new VerificationFragment())
                .commit();
    }

    @Nullable
    private Fragment next() {
        switch (instance.getStage()) {
            case 1:
                return new LoginFragment();
            case 2:
                return new VerificationFragment();
            case 3:
                return new ChooseDpFragment();
            case 4:
                return null;
            default:
                throw new AssertionError("invalid stage");
        }
    }

    public void onAddPhone() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, next())
                .commit();
    }

    public void onVerified(@NonNull User user) {
        Fragment next = next();
        if (next != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, next)
                    .commit();
        } else {
            onCompleted();
        }
    }

    public void onUpdateDp() {
        Fragment next = next();
        if (next != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, next)
                    .commit();
        } else {
            onCompleted();
        }
    }

    public void changeNumber() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, next())
                .commit();
    }

    public void onCompleted() {
        startActivity(new Intent(this, LauncherActivity.class));
        finish();
    }
}
