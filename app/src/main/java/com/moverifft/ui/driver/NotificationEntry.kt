package com.moverifft.ui.driver

import android.content.Context
import android.content.Intent
import android.support.annotation.LayoutRes
import android.text.format.DateUtils
import com.moverifft.R
import com.moverifft.ui.clients.TripDetailsActivity
import com.moverifft.ui.common.NotificationHolder
import com.moverifft.utils.GenericUtils
import com.squareup.picasso.Picasso
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by yaaminu on 10/29/17.
 */

open class NotificationEntry(
        @PrimaryKey
        var id: String,
        var text: String,
        var type: Int,
        private var timestamp: Long,
        private var details: String?,
        private var picUrl: String?,
        private var subTitle: String?,
        var json: String
) : RealmObject() {


    private fun toJson(): JSONObject {
        return try {
            JSONObject(json)
        } catch (e: JSONException) {
            throw RuntimeException()
        }
    }


    fun updateView(holder: NotificationHolder) {
        holder.notificationDate?.text = DateUtils.formatDateTime(holder.itemView.context, timestamp,
                DateUtils.FORMAT_SHOW_TIME or DateUtils.FORMAT_SHOW_DATE)
        holder.title?.text = text
        holder.subTitle?.text = subTitle
        holder.notificationText.text = details
        if (!GenericUtils.isEmpty(picUrl)) {
            Picasso.with(holder.itemView.context)
                    .load(picUrl)
                    .placeholder(R.drawable.ic_person_white_24dp)
                    .error(R.drawable.ic_person_white_24dp)
                    .into(holder.userAvatar)
        }
    }


    @Suppress("unused")
    constructor() : this("", "", 0, 0, "", "", "", "")

    fun getAction(context: Context): Intent? {
        try {
            val intent: Intent
            val jsonObject = toJson()
            when (type) {
                TYPE_TRIP_ENDED, TYPE_TRIP_STARTED -> {
                    intent = Intent(context, TripDetailsActivity::class.java)
                    intent.putExtra(TripDetailsActivity.EXTRA_TRIP, jsonObject.getString("tripId"))
                }
                else -> throw AssertionError()
            }
            return intent
        } catch (e: JSONException) {
            throw RuntimeException(e)
        }

    }

    companion object {
        val TYPE_TRIP_STARTED = 0x1
        val TYPE_TRIP_ENDED = 0x2
        val FIELD_TIMESTAMP = "timestamp"

        @LayoutRes
        fun getLayout(type: Int): Int {
            return when (type) {
                TYPE_TRIP_ENDED -> R.layout.trip_ended
                TYPE_TRIP_STARTED -> R.layout.trip_ended
                else -> R.layout.notification_list_item
            }
        }
    }
}
