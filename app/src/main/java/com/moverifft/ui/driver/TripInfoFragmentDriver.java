package com.moverifft.ui.driver;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.moverifft.R;
import com.moverifft.ui.clients.InfoFragment;
import com.moverifft.ui.clients.LatLang;
import com.moverifft.ui.clients.Trip;
import com.moverifft.ui.clients.TripDetailsActivity;
import com.moverifft.ui.clients.TripsApi;
import com.moverifft.utils.GenericUtils;

import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by yaaminu on 8/24/17.
 */

public class TripInfoFragmentDriver extends InfoFragment {

    private ProgressDialog progressDialog;

    @Override
    protected int getLayout() {
        return R.layout.fragment_trip_info_fragment_driver;
    }

    public static final String ARG_TRIP = "trip";

    public static TripInfoFragmentDriver create(@Nullable Trip trip) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(ARG_TRIP, trip);
        TripInfoFragmentDriver tripInfoFragmentClient = new TripInfoFragmentDriver();
        tripInfoFragmentClient.setArguments(bundle);
        return tripInfoFragmentClient;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
    }


    @OnClick(R.id.end_trip)
    void endTrip() {
        // TODO: 10/8/17 ask for confirmation
        TripsApi tripsApi = ((TripDetailsActivity) getActivity()).tripsApi;
        LatLang endLocation = ((TripDetailsActivity) getActivity()).getLocationNow();
        if (endLocation == null) {
            GenericUtils.showDialog(getContext(), "We were unable to obtain your location");
        } else {
            progressDialog.show();
            trip.setEndLocation(endLocation);
            tripsApi.endTrip(trip)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer);

        }
    }

    private final Observer<Trip> observer = new Observer<Trip>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            progressDialog.dismiss();
            GenericUtils.showDialog(getActivity(), e.getMessage());
        }

        @Override
        public void onNext(Trip trip) {
            progressDialog.dismiss();
            getActivity().finish();
        }
    };
}
