package com.moverifft.ui.driver

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import com.moverifft.BuildConfig

import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import java.util.concurrent.TimeUnit

/**
 * Created by yaaminu on 8/13/17.
 */

class LoginIntroFragment : BaseFragment() {

    override fun getLayout(): Int {
        return R.layout.login_intro
    }

    private val goToAddPhone = {
        (activity as LoginActivity).onAddPhone()
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view!!.findViewById<TextView>(R.id.version_name)
                .text = getString(R.string.version, BuildConfig.VERSION_NAME)
        view.postDelayed(goToAddPhone, TimeUnit.SECONDS.toMillis(5))
        view.setOnTouchListener({ _: View, _: MotionEvent ->
            view.removeCallbacks(goToAddPhone)
            goToAddPhone()
            true
        })
    }
}
