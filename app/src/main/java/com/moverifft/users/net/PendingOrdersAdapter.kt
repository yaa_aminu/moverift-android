package com.moverifft.users.net

import android.text.format.DateUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import com.moverifft.R
import com.moverifft.ui.clients.Order
import com.moverifft.ui.common.RecyclerViewBaseAdapter
import com.moverifft.utils.GenericUtils
import com.squareup.picasso.Picasso

/**
 * Created by yaaminu on 1/18/18.
 */


class PendingOrdersAdapter(private val del: Delegate) : RecyclerViewBaseAdapter<Order, OrderHolder>(del) {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OrderHolder {
        return OrderHolder(inflater.inflate(R.layout.pending_order, parent, false))
    }

    override fun doBindHolder(holder: OrderHolder?, position: Int) {
        val item = getItem(position)

        holder!!.pickupLocation.text = item.pickup?.name
        holder.destination.text = item.destination?.name
        holder.dateBooked.text = DateUtils.formatDateRange(del.context(), item.date, System.currentTimeMillis(),
                DateUtils.FORMAT_SHOW_DATE or DateUtils.FORMAT_SHOW_TIME or DateUtils.FORMAT_SHOW_WEEKDAY)
        holder.title.text = item.goods
        Picasso.with(del.context())
                .load(GenericUtils.getTruckTypeIcon(del.context(), item.truckType))
                .into(holder.truckTypeIcon)

    }


    interface Delegate : RecyclerViewBaseAdapter.Delegate<Order>

}

class OrderHolder(v: View) : RecyclerViewBaseAdapter.Holder(v) {
    @BindView(R.id.tv_pickup)
    lateinit var pickupLocation: TextView
    @BindView(R.id.tv_destination)
    lateinit var destination: TextView
    @BindView(R.id.title)
    lateinit var title: TextView
    @BindView(R.id.date_booked)
    lateinit var dateBooked: TextView
    @BindView(R.id.truck_type_icon)
    lateinit var truckTypeIcon: ImageView

}