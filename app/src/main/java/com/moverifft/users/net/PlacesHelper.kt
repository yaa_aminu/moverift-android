package com.moverifft.users.net

import com.moverifft.ui.clients.LatLang
import com.moverifft.utils.PLog
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import org.json.JSONObject
import rx.Observable
import java.io.IOException
import java.util.*

/**
 * Created by yaaminu on 1/9/18.
 */

private const val PLACES_API_KEY = "AIzaSyDSRPvxnc8WBJ7HbkqPqr0uniet91W2g_k"
private const val GEO_CODE_API_KEY = " AIzaSyApoO9lPIOMkCDj5Zh_8CiWi22e4clReX0"
private const val PLACES_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?&types=geocode&key=$PLACES_API_KEY"
private const val PLACES_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json?key=$PLACES_API_KEY"
private const val GEO_CODE_URL = "https://maps.googleapis.com/maps/api/geocode/json?key=$GEO_CODE_API_KEY"
private const val TAG = "PlacesHelper"
private val DEFAULT_LOCATION = LatLang(5.603716, -0.186964)

object PlacesHelper {
    private val client: OkHttpClient = OkHttpClient()

    fun searchPlace(place: String, currentUserLocation: LatLang?): Observable<List<LatLang>> {
        return Observable.create({
            it.onStart()
            try {
                if (place.isBlank()) {
                    it.onNext(geocode(currentUserLocation!!))
                } else {
                    val location = currentUserLocation ?: DEFAULT_LOCATION
                    val url = "$PLACES_URL&location=${location.latitude},${location.longitude}&input=$place&radius=50000"
                    val request = Request.Builder()
                            .url(
                                    url
                            ).build()
                    val response = client.newCall(request).execute()
                    if (response.isSuccessful) {
                        val string = response.body()?.string()
                        PLog.d(TAG, string)
                        it.onNext(processResponse(string!!))
                        it.onCompleted()
                    } else {
                        it.onError(Exception("Server error ${response.code()}"))
                    }
                }
            } catch (e: IOException) {
                it.onError(e)
            }
        })
    }


    private fun geocode(latLang: LatLang): List<LatLang> {
        val request = Request.Builder()
                .url(
                        "$GEO_CODE_URL&latlng=${latLang.latitude},${latLang.longitude}&result_type=street_address"
                ).build()
        val response = client.newCall(request).execute()
        val body = response.body()?.string()
        PLog.d(TAG, body)
        return processGeocodeResults(latLang, body!!)
    }

    private fun processGeocodeResults(latLang: LatLang, results: String): List<LatLang> {
        return try {
            val name = JSONObject(results).getJSONArray("results").getJSONObject(0)
                    .getString("formatted_address")
            Collections.singletonList(LatLang(latLang.latitude, latLang.longitude, name))
        } catch (e: JSONException) {
            PLog.e(TAG, e.message, e)
            emptyList()
        }
    }

    private fun processResponse(response: String): List<LatLang> {
        val array = JSONObject(response).getJSONArray("predictions")
        if (array.length() == 0) {
            return emptyList()
        }

        val ret = ArrayList<LatLang>(array.length())
        for (i in 0 until (array.length())) {
            val element = LatLang(0.0, 0.0, array.getJSONObject(i).getString("description"))
            element.placeId = array.getJSONObject(i).getString("place_id")
            ret.add(element)
        }
        return ret
    }

    fun findPlaceById(placeId: String): Pair<Double, Double> {
        val request = Request.Builder()
                .url(
                        "$PLACES_DETAILS_URL&placeid=$placeId"
                ).build()
        val response = client.newCall(request).execute()
        val body = response.body()?.string()
        PLog.d(TAG, body)
        val jsonObject = JSONObject(body).getJSONObject("result")
                .getJSONObject("geometry")
                .getJSONObject("location")
        return jsonObject.getDouble("lat") to jsonObject.getDouble("lng")
    }

}