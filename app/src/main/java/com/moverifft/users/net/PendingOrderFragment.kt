package com.moverifft.users.net

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.moverifft.MoveRifft
import com.moverifft.R
import com.moverifft.ui.clients.Order
import com.moverifft.ui.clients.TripsApi
import com.moverifft.ui.common.BaseFragment
import com.moverifft.ui.common.RecyclerViewBaseAdapter
import com.moverifft.users.data.UserApi
import com.moverifft.utils.PLog
import com.moverifft.utils.UiHelpers
import com.moverifft.utils.ViewUtils
import com.moverifft.utils.findAsync
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import kotlinx.android.synthetic.main.fragment_pending_order.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by yaaminu on 1/18/18.
 */
class PendingOrderFragment : BaseFragment() {

    companion object {
        val TAG = PendingOrderFragment::class.java.simpleName!!
    }

    var orders = emptyList<Order>()
    private lateinit var realm: Realm
    private lateinit var adapter: PendingOrdersAdapter

    @Inject
    lateinit var tripsApi: TripsApi
    @Inject
    lateinit var userApi: UserApi

    var loading = false

    override fun getLayout(): Int {
        return R.layout.fragment_pending_order
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MoveRifft.depGraph.inject(this)
        realm = Realm.getDefaultInstance()
        loading = true
        tripsApi.loadOrders()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loading = false
                    PLog.d(TAG, "loaded orders")
                    if (!isDestroyed) {
                        adapter.notifyDataChanged()
                    }
                }, {
                    loading = false
                    if (!isDestroyed) {
                        adapter.notifyDataChanged()
                        UiHelpers.showErrorDialog(context, it.message)
                    }

                })
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.layoutManager = LinearLayoutManager(context)
        adapter = PendingOrdersAdapter(delegate)
        recycler_view.adapter = adapter
        orders = realm.findAsync("date", Sort.DESCENDING)
        (orders as RealmResults).addChangeListener(onChange)
    }

    private val onChange: (realm: RealmResults<Order>) -> Unit = {
        adapter.notifyDataChanged()
    }

    private val delegate = object : PendingOrdersAdapter.Delegate {

        override fun onItemClick(adapter: RecyclerViewBaseAdapter<Order, *>?, view: View?, position: Int, id: Long) {
            val intent = Intent(context, OrderDetailsActivity::class.java)
            intent.putExtra(OrderDetailsActivity.EXTRA_ORDER, adapter!!.getItem(position))
            startActivity(intent)
        }

        override fun onItemLongClick(adapter: RecyclerViewBaseAdapter<Order, *>?, view: View?, position: Int, id: Long) = false

        override fun dataSet(): List<Order> {
            if (orders.isEmpty()) {
                ViewUtils.showViews(loading_layout)
                ViewUtils.showByFlag(loading, pb_loading)
                ViewUtils.showByFlag(!loading, empty_text)
                ViewUtils.hideViews(recycler_view)
            } else {
                ViewUtils.showViews(recycler_view)
                ViewUtils.hideViews(loading_layout)
            }
            return orders
        }

        override fun context() = context
    }

}