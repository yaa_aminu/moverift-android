package com.moverifft.users.net;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.GuardedBy;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.moverifft.BuildConfig;
import com.moverifft.common.Constants;
import com.moverifft.truck.data.Truck;
import com.moverifft.truck.data.TruckLocationReport;
import com.moverifft.truck.data.TruckManager;
import com.moverifft.ui.clients.LatLang;
import com.moverifft.ui.clients.Order;
import com.moverifft.ui.clients.Trip;
import com.moverifft.ui.clients.TripTrail;
import com.moverifft.ui.common.Callback;
import com.moverifft.utils.Config;
import com.moverifft.utils.GenericUtils;
import com.moverifft.utils.PLog;
import com.moverifft.utils.TaskManager;
import com.moverifft.utils.UpdateDpJob;
import com.moverifft.utils.VerifiedNumber;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sinch.verification.CodeInterceptionException;
import com.sinch.verification.InitiationResult;
import com.sinch.verification.SinchVerification;
import com.sinch.verification.Verification;
import com.sinch.verification.VerificationListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


/**
 * Created by yaaminu on 6/8/17.
 */

public class ParseBackend implements VerificationListener {
    private static final String TAG = "ParseBackend";
    private static final String FIELD_NAME_TRUCKS = "truck";
    private static final String PREF_NAMES_MAPPING_USER_ID = "names.mapping.userId.";
    private static final String PIN_CURRENT_TRIP = "current.trip";
    private static final String LOCATION_REPORT_PREFS = "location.report.prefs";

    @Nullable
    @GuardedBy("this")
    private ParseObject currentUserTruck;


    @NotNull
    public Observable<?> reportLocation(@NotNull final TruckLocationReport report) {
        return getCurrentUserTruck().flatMap(new Func1<ParseObject, Observable<?>>() {
            @Override
            public Observable<?> call(ParseObject parseObject) {
                return doUpdateLocation(report);
            }
        });
    }

    @NonNull
    private Observable<Object> doUpdateLocation(@NotNull final TruckLocationReport report) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                try {
                    Config.getPreferences(LOCATION_REPORT_PREFS)
                            .edit()
                            .putString(report.getTruckId(), report.toJson().toString())
                            .commit();
                    Map<String, Object> params = new HashMap<>(3);
                    params.put("lat", report.getLatitude());
                    params.put("lng", report.getLongitude());
                    params.put("timeReported", report.getTimeReported());
                    final Trip trip = doGetCurrentTrip();
                    if (trip != null) {
                        params.put("tripId", trip.getTripId());
                    }
                    ParseCloud.callFunction("reportLocation", params);
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    @NotNull
    public Observable<List<TruckLocationReport>> findAvailableTrucksLocations(final int truckType) {
        return Observable.create(new Observable.OnSubscribe<List<TruckLocationReport>>() {
            @Override
            public void call(Subscriber<? super List<TruckLocationReport>> subscriber) {
                try {
                    ParseQuery<ParseObject> query = getQueryForLocationReport(truckType);
                    List<ParseObject> parseObjects = query
                            .find();
                    List<TruckLocationReport> locationReports = new ArrayList<>(parseObjects.size());
                    for (ParseObject parseObject : parseObjects) {
                        locationReports.add(TruckLocationReport.create(parseObject));
                    }
                    subscriber.onNext(locationReports);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    private ParseQuery<ParseObject> getQueryForLocationReport(int truckType) {
        return ParseQuery.getQuery(FIELD_NAME_TRUCKS)
                .whereEqualTo(Constants.TRUCK_TYPE_IDENTIFIER, truckType)
                // TODO: 8/21/17 change the TimeUnit to Minutes
                // TODO: 8/21/17 change the the number of TimeUnit
                //todo add more fields to refine the query
                /*.selectKeys(Collections.singleton(Constants.LAT_LANG))*/;
    }

    public Observable<Trip> findTripById(@NonNull final String tripId) {
        return Observable.create(new Observable.OnSubscribe<Trip>() {
            @Override
            public void call(Subscriber<? super Trip> subscriber) {
                subscriber.onStart();
                try {
                    ParseObject trip = ParseQuery.getQuery("trip")
                            .get(tripId);

                    subscriber.onNext(Trip.create(trip));
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    @NotNull
    public Observable<Truck> findTruckById(@NotNull final String truckId) {
        return Observable.create(new Observable.OnSubscribe<Truck>() {
            @Override
            public void call(Subscriber<? super Truck> subscriber) {
                try {
                    subscriber.onStart();
                    ParseObject parseObject = ParseQuery.getQuery(FIELD_NAME_TRUCKS)
                            .get(truckId);

                    final Truck truck = TruckManager.create(parseObject);
                    if (truck != null) {
                        addMapping(truck.getOwnerId(), truck.getOwnerName());
                    }
                    subscriber.onNext(truck);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    subscriber.onError(e);
                }
            }
        });
    }


    public Observable<Order> initiateTrip(final Order order) {
        return Observable.create(new Observable.OnSubscribe<Order>() {
            @Override
            public void call(Subscriber<? super Order> subscriber) {
                subscriber.onStart();
                try {
                    ParseObject parseOrder = ParseObject.create("order");

                    GenericUtils.assertThat(order.getPickup() != null && order.getDestination() != null);
                    GenericUtils.assertThat(getCurrentUser() != null);
                    parseOrder.put("pickUp", order.getPickup().toJson().toString());
                    parseOrder.put("destination", order.getDestination().toJson().toString());
                    parseOrder.put("customerId", getCurrentUser().getObjectId());
                    parseOrder.put("customerEmail", getCurrentUser().getString(Constants.EMAIL_ADDRESS));
                    parseOrder.put("customerPhone", getCurrentUser().getString(Constants.PHONE_NUMBER));
                    parseOrder.put("truckType", order.getTruckType());
                    parseOrder.put("estimatedCost", order.getEstimatedCost());
                    parseOrder.put("goods", order.getGoods());
                    parseOrder.put("notes", order.getNotes());
                    parseOrder.put("paymentMode", order.getPaymentMode());

                    //server code must overwrite this
                    parseOrder.put("orderNumber", new SecureRandom().nextInt(10000) + "");

                    parseOrder.save();
                    order.setOrderId(parseOrder.getObjectId());
                    order.setDate(parseOrder.getCreatedAt().getTime());
                    order.setOrderNumber(parseOrder.getString("orderNumber"));
                    subscriber.onNext(order);
                    subscriber.onCompleted();
                } catch (JSONException e) {
                    subscriber.onError(e);
                } catch (ParseException e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public Observable<Trip> startTrip(final String hiredBy,
                                      final LatLang initialTruckLocation,
                                      final LatLang startLocation,
                                      final LatLang endLocation, final String goodsType) {
        // FIXME: 8/24/17 find a way to communicate the trip name
        return Observable.create(new Observable.OnSubscribe<Trip>() {
            @Override
            public void call(Subscriber<? super Trip> subscriber) {
                try {
                    Map<String, Object> params = new HashMap<>(3);
                    params.put("hiredBy", hiredBy);

                    params.put(Constants.START_LOCATION, startLocation.toJson().toString());

                    params.put(Constants.END_LOCATION, endLocation.toJson().toString());
                    params.put("goodsType", goodsType);
                    params.put(Constants.INITIAL_TRUCK_LOCATION, initialTruckLocation.toJson().toString());
                    //noinspection ConstantConditions
                    params.put("truckId", getCurrentUser().getString(Constants.TRUCK_ID));
                    ParseObject parseObject = ParseCloud.callFunction("startTrip", params);
                    parseObject.pin(PIN_CURRENT_TRIP);
                    subscriber.onNext(Trip.create(parseObject));
                    subscriber.onCompleted();
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage());
                    subscriber.onError(e);
                }
            }
        });
    }

    @Nullable
    private Trip doGetCurrentTrip() throws ParseException {
        List<ParseObject> parseObjects = ParseQuery.getQuery("trip")
                .fromPin(PIN_CURRENT_TRIP).find();
        if (parseObjects.isEmpty()) {
            return null;
        }
        return Trip.create(parseObjects.get(0));
    }

    public Observable<List<Trip>> findTripsForCurrentUser() {
        return Observable.create(new Observable.OnSubscribe<List<Trip>>() {
            @Override
            public void call(Subscriber<? super List<Trip>> subscriber) {
                subscriber.onStart();
                try {
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("trip");
                    final ParseObject currentUser = getCurrentUser();
                    GenericUtils.assertThat(currentUser != null);
                    if (BuildConfig.FLAVOR.equals(BuildConfig.FLAVOR_NAME_DRIVER)) {
                        query.whereEqualTo(Constants.TRUCK_ID, getCurrentUser().getString(Constants.TRUCK_ID));
                    } else {
                        query.whereEqualTo("hiredBy", currentUser.getObjectId());
                    }
                    List<ParseObject> results = query.find();
                    final List<Trip> trips = new ArrayList<>(results.size());
                    for (ParseObject result : results) {
                        trips.add(Trip.create(result));
                    }
                    subscriber.onNext(trips);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    public Observable<Trip> endTrip(final Trip trip) {
        return Observable.create(new Observable.OnSubscribe<Trip>() {
            @Override
            public void call(Subscriber<? super Trip> subscriber) {
                try {
                    subscriber.onStart();
                    Map<String, Object> params = new HashMap<>(3);
                    final LatLang endLocation = trip.getEndLocation();
                    GenericUtils.assertThat(endLocation != null);

                    params.put("tripId", trip.getTripId());
                    params.put("lat", endLocation.getLatitude());
                    params.put("long", endLocation.getLongitude());
                    final ParseObject endTrip = ParseCloud.callFunction("endTrip", params);
                    ParseQuery.getQuery("trip")
                            .fromPin(PIN_CURRENT_TRIP)
                            .get(trip.getTripId()).delete();
                    subscriber.onNext(Trip.create(endTrip));
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    subscriber.onError(e);
                    e.printStackTrace();
                }
            }
        });
    }


    @SuppressLint("ApplySharedPref")
    public void addMapping(String userId, String name) {
        Config.getPreferences(PREF_NAMES_MAPPING_USER_ID)
                .edit()
                .putString(userId, name)
                .commit();
    }

    @Nullable
    public String getNameFromId(String userId) {
        return Config.getPreferences(PREF_NAMES_MAPPING_USER_ID).getString(userId, userId);
    }

    public Observable<?> rejectRequest(final String userId, final String goodsType, final LatLang endLocation) {
        return Observable.from(TaskManager.execute(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Map<String, String> params = new HashMap<>(2);
                params.put("userId", userId);
                params.put("endLocation", endLocation.toJson().toString());
                params.put("goodsType", goodsType);
                ParseCloud.callFunction("rejectTripRequest", params);
                return true;
            }
        }, true));
    }

    @NonNull
    public Observable<Address> getLocationName(@NonNull final LatLang geoCoordinate) {
        return Observable.from(TaskManager.execute(new Callable<Address>() {
            @Override
            public Address call() throws Exception {
                Geocoder geocoder = new Geocoder(Config.getApplicationContext(), Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(geoCoordinate.getLatitude(),
                        geoCoordinate.getLongitude(), 1);
                if (addresses.isEmpty()) {
                    throw new Exception("No address associated with location");
                }
                return addresses.get(0);
            }
        }, true));
    }

    public void updateCurrentUserDp(final String path, final Callback<Boolean> callback) {
        getCurrentUserTruck()
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ParseObject>>() {
                    @Override
                    public Observable<? extends ParseObject> call(Throwable throwable) {
                        return Observable.just(null);
                    }
                })
                .map(new Func1<ParseObject, Boolean>() {
                    @Override
                    public Boolean call(ParseObject parseObject) {
                        if (parseObject != null) {
                            synchronized (ParseBackend.this) {
                                if (currentUserTruck == null) {
                                    currentUserTruck = parseObject;
                                }
                                currentUserTruck.put("dp", path);
                            }
                        }
                        TaskManager.runJob(UpdateDpJob.create(path, ((ParseUser) getCurrentUser())));
                        return true;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean successs) {
                        callback.done(null, successs);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        PLog.e(TAG, throwable.getMessage(), throwable);
                        callback.done(new Exception(throwable), false);
                    }
                });
    }

    @NotNull
    public Observable<Truck> markAvailable(@NotNull final String truckId, final boolean available) {
        return Observable.create(new Observable.OnSubscribe<Truck>() {
            @Override
            public void call(Subscriber<? super Truck> subscriber) {
                try {
                    ParseObject parseObject = ParseQuery.getQuery("truck")
                            .get(truckId);
                    parseObject.put("available", available);
                    parseObject.save();
                    Truck truck = TruckManager.create(parseObject);
                    if (truck != null) {
                        addMapping(truck.getOwnerId(), truck.getOwnerName());
                    }
                    subscriber.onNext(truck);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    PLog.f(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    @NotNull
    public Observable<List<LatLang>> searchPlace(@NotNull final String name, final LatLang currentLocation) {
        return Observable.just(name)
                .flatMap(new Func1<String, Observable<List<LatLang>>>() {
                    @Override
                    public Observable<List<LatLang>> call(String placeName) {
                        return PlacesHelper.INSTANCE
                                .searchPlace(placeName, currentLocation);
                    }
                });
    }


    private final Map<String, String> dpCache = new ConcurrentHashMap<>(5);

    @Nullable
    public String getUserDpNonBlocking(final String userId) {
        String dp = dpCache.get(userId);
        if (GenericUtils.isEmpty(dp) && !dpCache.containsKey(userId)) {
            dpCache.put(userId, ""); //so that we only fetch it once. see below how we clear it
            //noinspection ConstantConditions
            if (getCurrentUser().getObjectId().equals(userId)) {
                dp = getCurrentUser().getString("dp");
                dpCache.put(userId, dp);
            } else {
                TaskManager.execute(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (ParseBackend.this) {
                            try {
                                String dp = doGetDp(userId);
                                dpCache.put(userId, dp);
                            } catch (ParseException e) {
                                PLog.e(TAG, e.getMessage(), e);
                                dpCache.remove(userId);//allow trying to fetch again
                            }
                        }
                    }
                }, true);
            }
        }
        return dp;
    }

    private String doGetDp(String userId) throws ParseException {
        return ParseQuery.getQuery(ParseUser.class)
                .selectKeys(Collections.singleton("dp"))
                .get(userId).getString("dp");
    }

    public Observable<String> getUserDp(final String userId) {
        return Observable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                String dp = dpCache.get(userId);
                if (dp == null) {
                    dp = doGetDp(userId);
                    dpCache.put(userId, dp);
                }
                return dp;
            }
        });
    }

    @NotNull
    public Observable<List<Order>> loadOrders() {
        return Observable.just(BuildConfig.FLAVOR.equals(BuildConfig.FLAVOR_NAME_CLIENT))
                .flatMap(new Func1<Boolean, Observable<List<ParseObject>>>() {
                    @Override
                    public Observable<List<ParseObject>> call(Boolean isClientBuild) {
                        try {
                            GenericUtils.assertThat(getCurrentUser() != null);
                            List<ParseObject> orders = ParseQuery.getQuery("order")
                                    .whereEqualTo("customerId", getCurrentUser().getObjectId())
                                    .find();
                            return Observable.just(orders);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return Observable.error(e);
                        }
                    }
                }).map(new Func1<List<ParseObject>, List<Order>>() {
                    @Override
                    public List<Order> call(List<ParseObject> parseObjects) {
                        List<Order> orders = new ArrayList<>(parseObjects.size());
                        for (ParseObject parseObject : parseObjects) {
                            orders.add(Order.create(parseObject));
                        }
                        return orders;
                    }
                });
    }

    public Observable<Object> resendVerificationCode() {
        return resendVerificationCode(getCurrentUser().getString(Constants.PHONE_NUMBER));
    }

    public Observable<Object> resendVerificationCode(final String number) {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                try {
                    sendVerificationCode(number);
                    subscriber.onNext(new Object());
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    public Observable<VerifiedNumber> verifyPaymentWallet(@NonNull final String num, @NotNull final String walletType, @NonNull final String verificationCode) {
        return Observable.create(new Observable.OnSubscribe<VerifiedNumber>() {
            @Override
            public void call(Subscriber<? super VerifiedNumber> subscriber) {
                if (verification == null) {
                    subscriber.onError(new IllegalStateException("Internal Error"));
                } else if (!num.equals(number)) {
                    subscriber.onError(new IllegalStateException("Internal Error"));
                } else {
                    TaskManager.executeOnMainThread(new Runnable() {
                        @Override
                        public void run() {
                            verification.verify(verificationCode);
                        }
                    });
                    try {
                        countDownLatch.await();
                        if (verificationFailed != null) {
                            subscriber.onError(verificationFailed);
                        }
                        ParseObject verifiedNumber = ParseObject.create("wallet");
                        ParseUser currentUser = getCurrentUser();
                        GenericUtils.assertThat(currentUser != null);
                        verifiedNumber.put("number", num);
                        verifiedNumber.put("ownerName", currentUser.getString(Constants.NAME));
                        verifiedNumber.put("type", walletType);
                        verifiedNumber.put("ownerId", currentUser.getObjectId());
                        verifiedNumber.save();
                        subscriber.onNext(new VerifiedNumber(num, walletType, verifiedNumber.getUpdatedAt().getTime()));
                    } catch (InterruptedException | ParseException e) {
                        PLog.e(TAG, e.getMessage(), e);
                        subscriber.onError(e);
                    }
                }
            }
        });
    }

    public Observable<List<VerifiedNumber>> loadVerifiedNumbers(final long lastUpdated) {
        return Observable.create(new Observable.OnSubscribe<List<ParseObject>>() {
            @Override
            public void call(Subscriber<? super List<ParseObject>> subscriber) {
                try {
                    if (!subscriber.isUnsubscribed()) {
                        List<ParseObject> numbers = ParseQuery.getQuery("wallet")
                                .whereEqualTo("ownerId", getCurrentUser().getObjectId())
                                .whereGreaterThan("updatedAt", new Date(lastUpdated))
                                .addAscendingOrder("updatedAt")
                                .find();
                        subscriber.onNext(numbers);
                        subscriber.onCompleted();
                    }
                } catch (ParseException e) {
                    subscriber.onError(e);
                }
            }
        }).filter(new Func1<List<ParseObject>, Boolean>() {
            @Override
            public Boolean call(List<ParseObject> parseObjects) {
                return !parseObjects.isEmpty();
            }
        }).map(new Func1<List<ParseObject>, List<VerifiedNumber>>() {
            @Override
            public List<VerifiedNumber> call(List<ParseObject> parseObjects) {
                List<VerifiedNumber> numbers = new ArrayList<>(parseObjects.size());
                for (ParseObject parseObject : parseObjects) {
                    numbers.add(new VerifiedNumber(parseObject));
                }
                return numbers;
            }
        });
    }

    private static class Holder {
        private static ParseBackend instance = new ParseBackend();
    }

    public static ParseBackend getInstance() {
        return Holder.instance;
    }


    @Nullable
    public ParseUser getCurrentUser() {
        return ParseUser.getCurrentUser();
    }

    public Observable<ParseUser> loginOrSignup(final String name, final String phoneNumber, final @NonNull String email) {
        return Observable.create(new Observable.OnSubscribe<ParseUser>() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void call(final Subscriber<? super ParseUser> subscriber) {
                subscriber.onStart();
                try {
                    ParseUser parseUser = doLoginOrSignup(phoneNumber, name, email);
                    sendVerificationCode(phoneNumber);
                    subscriber.onNext(parseUser);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    // TODO: 10/31/17 obfuscate this api key
    public static final String APPLICATION_KEY = "465fcda7-d6df-4b6c-981c-ff4d06879653";

    private com.sinch.verification.Config config = SinchVerification.config()
            .applicationKey(APPLICATION_KEY)
            .context(Config.getApplicationContext())
            .build();

    @Nullable
    @GuardedBy("this")
    private String number;
    private Verification verification;
    @GuardedBy("this")
    CountDownLatch countDownLatch = new CountDownLatch(1);
    @GuardedBy("this")
    volatile ParseException verificationFailed;

    public synchronized void sendVerificationCode(final String userId) throws ParseException {
        verificationFailed = null;
        TaskManager.executeOnMainThread(new Runnable() {
            @Override
            public void run() {
                initialiseVerification(userId);
                verification.initiate();
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            verificationFailed = new ParseException(e);
        }
        if (verificationFailed != null) {
            throw verificationFailed;
        }
    }

    private void initialiseVerification(String userId) {
        verification = SinchVerification.createSmsVerification(config, "+" + userId, this);
        this.number = userId;
    }

    @Override
    public void onInitiated(InitiationResult initiationResult) {
        //initiated
        countDownLatch.countDown();
    }

    @Override
    public void onInitiationFailed(Exception e) {
        //report error
        verificationFailed = new ParseException(e);
        countDownLatch.countDown();
    }

    @Override
    public void onVerified() {
        //mark current user as verified
        verificationFailed = null;
        countDownLatch.countDown();
    }

    @Override
    public void onVerificationFailed(Exception e) {
        //report error
        if (!(e instanceof CodeInterceptionException)) {
            verificationFailed = new ParseException(e);
            countDownLatch.countDown();
        } else {
            verificationFailed = null;
        }
    }

    @NonNull
    private ParseUser doLoginOrSignup(@NonNull String phoneNumber, @NonNull String name, @NonNull String email) throws ParseException {
        final char[] password = phoneNumberToPassword(phoneNumber);
        try {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser == null) {
                currentUser = ParseUser.logIn(phoneNumber, String.valueOf(password));
                currentUser.put(Constants.IS_VERIFIED, false);
            }

            boolean changed = false;
            if (currentUser.getBoolean(Constants.IS_VERIFIED)) {
                changed = true;
                currentUser.put(Constants.IS_VERIFIED, false);
            }
            if (!email.equals(currentUser.getString(Constants.EMAIL_ADDRESS))) {
                changed = true;
                currentUser.put(Constants.EMAIL_ADDRESS, email);
            }
            if (!name.equals(currentUser.getString(Constants.NAME))) {
                changed = true;
                currentUser.put(Constants.NAME, name);
            }
            if (changed) {
                currentUser.save();
            }
            return currentUser;
        } catch (ParseException e) {
            PLog.d(TAG, e.getMessage(), e);
            if (e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                return doSignUp(phoneNumber, password, name, email);
            }
            throw e;
        }

    }

    private char[] phoneNumberToPassword(@NonNull String phoneNumber) {
        PLog.w(TAG, "using fake password generator");
        return phoneNumber.toCharArray();
    }

    @NonNull
    private ParseUser doSignUp(@NonNull String phoneNumber, char[] password, String name, String email) throws ParseException {
        ParseUser parseUser = new ParseUser();
        parseUser.setUsername(phoneNumber);
        parseUser.put(Constants.NAME, name);
        parseUser.put(Constants.IS_VERIFIED, false);
        parseUser.put(Constants.PHONE_NUMBER, phoneNumber);
        parseUser.put(Constants.EMAIL_ADDRESS, email);
        parseUser.setPassword(String.valueOf(password));
        parseUser.signUp();
        return ParseUser.getCurrentUser();
    }

    @NotNull
    public Observable<ParseUser> verify(@NotNull final String verificationCode) {
        initialiseVerification(getCurrentUser().getString("phoneNumber"));
        return Observable.create(new Observable.OnSubscribe<ParseUser>() {
            @SuppressLint("ApplySharedPref")
            @Override
            public void call(Subscriber<? super ParseUser> subscriber) {
                synchronized (ParseBackend.this) {
                    try {
                        if (verification == null) {
                            throw new AssertionError();
                        }
                        TaskManager.executeOnMainThread(new Runnable() {
                            @Override
                            public void run() {
                                verification.verify(verificationCode);
                            }
                        });
                        countDownLatch.await();
                        if (verificationFailed != null) {
                            throw verificationFailed;
                        }
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        GenericUtils.assertThat(currentUser != null, "must be logged in");
                        currentUser.put(Constants.IS_VERIFIED, true);
                        currentUser.save();
                        final ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
                        currentInstallation.put(Constants.USER_ID, currentUser.getObjectId());
                        currentInstallation
                                .saveInBackground();
                        subscriber.onNext(currentUser);
                        subscriber.onCompleted();
                    } catch (ParseException | InterruptedException e) {
                        subscriber.onError(e);
                    }
                }
            }
        });
    }

    @NotNull
    public Observable<Truck> addTruck(final String vehicleNO) {
        return getCurrentUserTruck()
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ParseObject>>() {
                    @Override
                    public Observable<? extends ParseObject> call(Throwable throwable) {
                        if (throwable instanceof ParseException && ((ParseException) throwable).getCode() == ParseException.OBJECT_NOT_FOUND) {
                            return Observable.just(ParseObject.create(FIELD_NAME_TRUCKS));
                        }
                        return Observable.error(throwable);
                    }
                })
                .flatMap(new Func1<ParseObject, Observable<Truck>>() {
                    @Override
                    public Observable<Truck> call(ParseObject parseObject) {
                        return doAddTruck(parseObject, vehicleNO);
                    }
                }).doOnNext(new Action1<Truck>() {
                    @Override
                    public void call(Truck truck) {
                        try {
                            final ParseObject currentUser = getCurrentUser();
                            assert currentUser != null;
                            currentUser.put(Constants.TRUCK_ID, truck.getTruckId());
                            currentUser.save();
                        } catch (ParseException e) {
                            throw Exceptions.propagate(e);
                        }
                    }
                });
    }


    public rx.Observable<ParseObject> getCurrentUserTruck() {
        return rx.Observable.create(new Observable.OnSubscribe<ParseObject>() {
            @Override
            public void call(Subscriber<? super ParseObject> subscriber) {
                try {
                    synchronized (ParseBackend.this) {
                        if (currentUserTruck == null) {
                            final ParseObject currentUser = getCurrentUser();
                            GenericUtils.assertThat(currentUser != null);
                            currentUserTruck = ParseQuery.getQuery(FIELD_NAME_TRUCKS)
                                    .whereEqualTo(Constants.OWNER_ID, currentUser.getObjectId())
                                    .getFirst();
                        } else {
                            String json = Config.getPreferences(LOCATION_REPORT_PREFS)
                                    .getString(currentUserTruck.getObjectId(), null);
                            if (json != null) {
                                TruckLocationReport locationReport = TruckLocationReport.fromJson(json);
                                currentUserTruck.put(Constants.LAT_LANG, new ParseGeoPoint(locationReport.getLatitude(),
                                        locationReport.getLongitude()));
                            }
                        }
                    }
                    subscriber.onNext(currentUserTruck);
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    private Observable<Truck> doAddTruck(final ParseObject startWith, final String vehicleNO) {
        return Observable.create(new Observable.OnSubscribe<Truck>() {
            @Override
            public void call(Subscriber<? super Truck> subscriber) {
                try {
                    ParseObject currentUser = getCurrentUser();
                    assert currentUser != null;
                    startWith.put(Constants.OWNER_ID, currentUser.getObjectId());
                    startWith.put(Constants.OWNER_NAME, currentUser.getString(Constants.NAME));
                    startWith.put(Constants.AVAILABLE, false);
                    startWith.put(Constants.REG_NO, vehicleNO);
                    String dp = currentUser.getString("dp");
                    startWith.put("ownerDp", GenericUtils.isEmpty(dp) ? "empty" : dp);
                    startWith.put(Constants.TRUCK_VERIFIED, false);
                    startWith.put(Constants.TRUCK_TYPE_IDENTIFIER, 0);
                    subscriber.onStart();

                    // TODO: 8/19/17 run a server side hook to check duplicate entries
                    startWith.save();


                    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                    installation.put(Constants.TRUCK_ID, startWith.getObjectId());
                    installation.saveInBackground();

                    synchronized (ParseBackend.this) {
                        currentUserTruck = startWith;
                    }
                    subscriber.onNext(TruckManager.create(startWith));
                    subscriber.onCompleted();
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    @NotNull
    public Observable<JSONObject> getRoutes(final LatLang from, final LatLang to) {
        return Observable.create(new Observable.OnSubscribe<JSONObject>() {
            @Override
            public void call(Subscriber<? super JSONObject> subscriber) {
                try {
                    Map<String, Object> params = new HashMap<>(2);
                    params.put("from", from.toJson().toString());
                    params.put("to", to.toJson().toString());
                    Map<String, ?> ret = ParseCloud.callFunction("getRoute", params);
                    JSONObject jsonObject = new JSONObject(ret);
                    PLog.d(TAG, jsonObject.toString(2));
                    subscriber.onNext(jsonObject);
                    subscriber.onCompleted();
                } catch (JSONException e) {
                    PLog.d(TAG, e.getMessage(), e);
                    throw new RuntimeException(e.getCause());
                } catch (ParseException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }

    @NotNull
    public Observable<Object> logout() {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                subscriber.onStart();

                if (ParseUser.getCurrentUser() == null) {
                    subscriber.onNext(true);
                }
                try {
                    bolts.Task task = ParseUser.logOutInBackground();
                    task.waitForCompletion();
                    if (task.getError() != null) {
                        subscriber.onError(task.getError());
                    } else {
                        subscriber.onNext(true);
                        subscriber.onCompleted();
                    }
                } catch (InterruptedException e) {
                    PLog.e(TAG, e.getMessage(), e);
                    subscriber.onError(e);
                }
            }
        });
    }


    public Observable<ParseObject> updateTruckType(final int truckType) {
        return getCurrentUserTruck()
                .flatMap(new Func1<ParseObject, Observable<ParseObject>>() {
                    @Override
                    public Observable<ParseObject> call(final ParseObject truck) {
                        return Observable.create(new Observable.OnSubscribe<ParseObject>() {
                            @Override
                            public void call(Subscriber<? super ParseObject> subscriber) {
                                try {
                                    subscriber.onStart();
                                    ParseUser currentUser = ParseUser.getCurrentUser();
                                    GenericUtils.assertThat(currentUser != null, "must be logged in");
                                    truck.put(Constants.TRUCK_TYPE_IDENTIFIER, truckType);
                                    truck.save();
                                    subscriber.onNext(truck);
                                    subscriber.onCompleted();
                                } catch (ParseException e) {
                                    subscriber.onError(e);
                                }
                            }
                        });
                    }
                });
    }

    public Observable<List<TripTrail>> getTrail(final String tripId) {
        return Observable.from(TaskManager.execute(new Callable<ParseObject>() {
            @Override
            public ParseObject call() throws Exception {
                // TODO: 10/17/17 use the local data if we are running the driver client

                return ParseQuery.getQuery("trip")
                        .selectKeys(Collections.singletonList("trail"))
                        .get(tripId);

            }
        }, true)).map(new Func1<ParseObject, List<TripTrail>>() {
            @Override
            public List<TripTrail> call(ParseObject parseObject) {
                List<String> trails = parseObject.getList("trail");
                if (trails == null) return Collections.emptyList();
                List<TripTrail> tripTrails = new ArrayList<>(trails.size());
                for (String trail : trails) {
                    tripTrails.add(TripTrail.fromJson(parseObject.getObjectId(), trail));
                }
                Collections.sort(tripTrails, new Comparator<TripTrail>() {
                    @Override
                    public int compare(TripTrail lhs, TripTrail rhs) {
                        if (lhs.timestamp > rhs.timestamp) return 1;
                        if (lhs.timestamp < rhs.timestamp) return -1;
                        return 0;
                    }
                });
                // FIXME: 10/17/17 remove this
                strech(tripTrails);
                ////////////////////////////////

                return tripTrails;
            }
        });
    }

    private static void strech(List<TripTrail> trails) {
        if (trails.size() <= 1) return;
        for (int i = 0; i < trails.size(); i++) {
            if (i < trails.size() - 1) {
                double r_earth = 6371000;
                final TripTrail nextTrail = trails.get(i + 1);
                final LatLang next = nextTrail.location;
                if (trails.get(i).location.distanceInMetersTo(next) < 5000) {
                    double lat = next.getLatitude() + (5000 / r_earth) * (180 / Math.PI);
                    LatLang latLang = new LatLang(lat, next.getLongitude());
                    trails.set(i + 1, new TripTrail(nextTrail.tripId, nextTrail.timestamp, latLang));
                }
            }
        }
    }
}
