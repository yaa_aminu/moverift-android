package com.moverifft.users.net

import android.os.Bundle
import com.moverifft.R
import com.moverifft.ui.clients.Order
import com.moverifft.ui.clients.OrderDetailsFragment
import com.moverifft.ui.driver.BaseActivity


class OrderDetailsActivity : BaseActivity(), OrderDetailsFragment.OrderProvider {
    companion object {
        const val EXTRA_ORDER = "extra_order"
    }

    private lateinit var currentOrder: Order

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentOrder = intent.getParcelableExtra(EXTRA_ORDER)
        setContentView(R.layout.activity_order_details)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun getOrder(): Order {
        return currentOrder
    }
}