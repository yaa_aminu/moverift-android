package com.moverifft.utils

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.ui.driver.BaseActivity
import com.moverifft.users.data.UserManager
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_verified_numbers.*
import java.util.*

const val ACTION = "action"
const val ADD_NUMBER = "add"

class VerifiedNumbersActivity : BaseActivity(), OnNumberAdded, VerifyWalletNumberFragment.VerificationListener {
    override fun onVerificationCodeSent(num: VerifiedNumber) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, VerifyWalletNumberFragment()
                        .apply {
                            val bundle = Bundle(1)
                            bundle.putParcelable(NUM, num)
                            arguments = bundle
                        }, "add phone number")
                .addToBackStack("back")
                .commit()
    }

    override fun onVerified(num: VerifiedNumber) {
        supportFragmentManager
                .popBackStack("back", FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verified_numbers)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, VerifiedNumberFragment(), "verified numbers")
                .commit()
        if (ADD_NUMBER == intent.getStringExtra(ACTION)) {
            Handler().postDelayed({
                addPhoneNumber(fab)
            }, 750)
        }
    }

    val addPhoneNumber: (_: View) -> Unit = {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, AddPhoneNumberFragment(), "add phone number")
                .addToBackStack("back")
                .commit()
    }
}

class VerifiedNumberFragment : BaseFragment() {
    var numbers: List<VerifiedNumber> = emptyList<VerifiedNumber>()
    lateinit var realm: Realm
    lateinit var adapter: ArrayAdapter<VerifiedNumber>

    override fun getLayout(): Int {
        return R.layout.fragment_verified_numbers
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        realm = Realm.getDefaultInstance()
        UserManager.getInstance()
                .loadVerifiedNumbers()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = VerifiedNumbersAdapter()
        list.emptyView = empty
        list.adapter = adapter

        fab.setOnClickListener((context as VerifiedNumbersActivity).addPhoneNumber)
        numbers = realm.findAsync("num")
        (numbers as RealmResults)
                .addChangeListener(changeListener)
    }

    override fun onDestroyView() {
        if (numbers is RealmResults) {
            val num = numbers as RealmResults<VerifiedNumber>
            num.removeChangeListener(changeListener)
        }
        super.onDestroyView()
    }

    override fun onDestroy() {
        realm.close()
        super.onDestroy()
    }

    private val changeListener = { ret: RealmResults<VerifiedNumber> ->
        numbers = ret
        adapter.notifyDataSetChanged()
    }

    inner class VerifiedNumbersAdapter : ArrayAdapter<VerifiedNumber>(context, R.layout.payment_option_list_item, Collections.emptyList()) {

        override fun getItem(position: Int): VerifiedNumber {
            return numbers[position]
        }

        override fun getCount(): Int {
            return numbers.size
        }

        override fun getView(position: Int, v: View?, parent: ViewGroup?): View {
            val convertView = v ?: LayoutInflater.from(context)
                    .inflate(R.layout.verified_number_list_item, parent, false)

            (convertView.findViewById(R.id.tv_wallet) as TextView).text = getItem(position).walletType
            (convertView.findViewById(R.id.tv_number) as TextView).text = getItem(position).localNum
            return convertView
        }
    }
}

