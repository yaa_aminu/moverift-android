package com.moverifft.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.moverifft.R;
import com.moverifft.ui.clients.LabeledLocation;
import com.moverifft.ui.clients.LatLang;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * author Null-Pointer on 1/11/2016.
 */
@SuppressWarnings({"unused", "SameParameterValue"})
public class GenericUtils {
    private GenericUtils() {
    }

    public static void ensureNotNull(Object... o) {
        if (o == null) throw new IllegalArgumentException("null");
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < o.length; i++) {
            if (o[i] == null) {
                throw new IllegalArgumentException("null");
            }
        }
    }

    public static void ensureNotNull(Object o, String message) {
        ensureNotNull(message);
        if (o == null) throw new IllegalArgumentException(message);
    }

    @NonNull
    public static Circle drawCurrentLocationCircle(@NonNull GoogleMap googleMap,
                                                   @NonNull LatLang currentLocation, @Nullable Circle circle) {
        if (circle != null) {
            circle.setCenter(currentLocation.toLatLng());
        } else {
            CircleOptions circleOptions = new CircleOptions()
                    .center(currentLocation.toLatLng())
                    .radius(300.0)
                    .clickable(false)
                    .fillColor(Color.parseColor("#552e7d32"))
                    .strokeColor(Color.parseColor("#552e7d32"));
            circle = googleMap.addCircle(circleOptions);

            if (googleMap.getCameraPosition().zoom < 14) {
                CameraPosition cameraUpdate = new CameraPosition.Builder()
                        .target(currentLocation.toLatLng())
                        .zoom(16f).tilt(20f)
                        .bearing(0f)
                        .build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraUpdate));
            }
        }
        return circle;
    }

    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.toString().trim().length() == 0;
    }

    public static String getString(@StringRes int res) {
        return Config.getApplicationContext().getString(res);
    }

    public static String getString(@StringRes int res, Object... args) {
        return Config.getApplicationContext().getString(res, args);
    }

    public static void ensureNotEmpty(String... args) {
        if (args == null) throw new IllegalArgumentException("null");
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < args.length; i++) {
            if (isEmpty(args[i])) {
                throw new IllegalArgumentException("null");
            }
        }
    }

    public static void ensureConditionTrue(boolean condition, String message) {
        message = message == null ? "" : message;
        if (!condition)
            throw new IllegalArgumentException(message);
    }

    public static boolean isCapitalised(String text) {
        ensureNotNull(text);
        return text.equals(capitalise(text));
    }

    public static String capitalise(String text) {
        text = text != null ? text : "";
        if (isEmpty(text.trim())) return text;
        StringBuilder builder = new StringBuilder(text);
        boolean previousWasSpace = true; //capitalize sentence
        //we assume that the string is trimmed
        for (int i = 0; i < builder.length(); i++) {
            char c = builder.charAt(i);
            if (previousWasSpace) {//don't check whether char is letter or not
                builder.setCharAt(i, Character.toUpperCase(c));
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
            previousWasSpace = Character.isSpaceChar(c);
        }
        return builder.toString();
    }

    public static String cleanNumberText(String text) {
        return text.replaceAll("[^\\d\\.]+", "");
    }


    public static void showComfirmationDialog(Context context, String message, final Runnable cb) {
        DialogInterface.OnClickListener onclickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    cb.run();
                }
            }
        };
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, onclickListener)
                .setNegativeButton(android.R.string.cancel, null)
                .create().show();
    }

    public static void assertThat(boolean test) {
        assertThat(test, "");
    }

    public static void assertThat(boolean test, String message) {
        if (!test) {
            throw new AssertionError(message);
        }
    }

    public static void showDialog(Context context, String message) {
        showDialog(context, message, null);
    }

    public static void showDialog(Context context, String message, final @Nullable Runnable cb) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (cb != null) {
                    cb.run();
                }
            }
        });
        builder.create()
                .show();
    }

    public static final NumberFormat FORMAT = DecimalFormat.getNumberInstance();

    static {
        GenericUtils.FORMAT.setMaximumFractionDigits(2);
        GenericUtils.FORMAT.setRoundingMode(RoundingMode.HALF_UP);
    }

    public static String formatDistance(double num) {
        return FORMAT.format(num);
    }


    public static Bitmap vectorToDrawable(Context context, @DrawableRes int res) {
        Drawable carMarkerDrawable =
                ContextCompat.getDrawable(context,
                        res);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            carMarkerDrawable = DrawableCompat.wrap(carMarkerDrawable).mutate();
        }
        carMarkerDrawable.setBounds(0, 0, carMarkerDrawable.getIntrinsicWidth(), carMarkerDrawable.getIntrinsicHeight());

        Bitmap carMarker = Bitmap.createBitmap(carMarkerDrawable.getIntrinsicWidth(),
                carMarkerDrawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888);
        carMarkerDrawable.draw(new Canvas(carMarker));
        return carMarker;
    }

    public static boolean hasPermission(FragmentActivity appCompatActivity, boolean askIfNotGranted, int requestCode, String... permissions) {
        assertThat(permissions.length > 0);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        boolean hasPermission = true;
        for (String permission : permissions) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(appCompatActivity, permission)) {
                hasPermission = false;
                break;
            }
        }
        if (!hasPermission && askIfNotGranted) {
            appCompatActivity.requestPermissions(permissions, requestCode);
        }
        return hasPermission;
    }

    public static Bitmap loadBitmap(String path, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inJustDecodeBounds = false;
        double scaleFactor = Math.ceil(Math.min(options.outWidth * 1.0 / width,
                options.outHeight * 1.0 / height * 1.0));
        options.inSampleSize = (int) scaleFactor;
        if (options.inSampleSize < 1) {
            options.inSampleSize = 1;
        }
        return BitmapFactory.decodeFile(path, options);
    }

    public static String formatTimeStamp(long duration) {
        long totalSeconds = duration / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        if (minutes >= 60) {
            return String.format(Locale.US, "%02d:%02d:%02d", (int) (minutes / 60), minutes % 60, seconds);
        } else {
            return String.format(Locale.US, "%02d:%02d", minutes, seconds);
        }
    }

    public static boolean wasPermissionGranted(String[] permissions, int[] grantResults) {
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    public static void fitMapToBounds(@NonNull GoogleMap googleMap, int padding, LatLang... latLangs) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (LatLang latLang : latLangs) {
            builder.include(latLang.toLatLng());
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), padding);
        googleMap.animateCamera(cameraUpdate);
    }

    public static void fitMapToBounds(@NonNull GoogleMap googleMap, int padding, List<LabeledLocation> latLangs) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (LabeledLocation latLang : latLangs) {
            builder.include(new LatLng(latLang.lat, latLang.lng));
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), padding);
        googleMap.animateCamera(cameraUpdate);
    }

    public static void loadDp(@Nullable String dp, ImageView imageView) {
        if (dp != null) {
            final RequestCreator creator;
            if (new File(dp).exists()) {
                creator = Picasso.with(imageView.getContext())
                        .load(new File(dp));
            } else {
                creator = Picasso.with(imageView.getContext())
                        .load(dp);
            }
            creator.placeholder(R.drawable.ic_person_white_24dp)
                    .error(R.drawable.ic_person_white_24dp)
                    .into(imageView);
        }
    }

    public static String getTruckTypeName(Context context, int truckTypeIdentifier) {
        String prefix;
        if (truckTypeIdentifier < 20) {
            prefix = getString(R.string.light_duty_trucks) + " ";
        } else if (truckTypeIdentifier < 40) {
            prefix = getString(R.string.long_haul_trucks) + " ";
        } else if (truckTypeIdentifier < 60) {
            prefix = getString(R.string.project_haul) + " ";
        } else {
            throw new AssertionError();
        }
        return prefix + context.getString(
                context.getResources().getIdentifier("truck_type_"
                        + truckTypeIdentifier, "string", context.getPackageName()));
    }

    @DrawableRes
    public static int getTruckTypeIcon(@NotNull Context context, int truckType) {
        return context.getResources().getIdentifier("truck_" + truckType,
                "drawable", context.getPackageName());
    }

    private static Pattern pattern = null;

    public static boolean isValidEmail(@NotNull CharSequence email) {
        if (pattern == null) {
            pattern = Pattern.compile("^\\w[\\w\\-_.]+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$", Pattern.CASE_INSENSITIVE);
        }
        return pattern.matcher(email).matches();
    }

    private static final String TAG = "GenericUtils";

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("applySharedPref")
    public static void clearSharedPrefs(Context context) {
        File prefsDir = new File(context.getFilesDir().getParentFile(), "shared_prefs");
        PLog.d(TAG, "deleting shared preferences");
        for (File file : prefsDir.listFiles()) {
            context.getSharedPreferences(file.getName().split("\\.")[0], Context.MODE_PRIVATE)
                    .edit()
                    .clear()
                    .commit();
            file.delete();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @SuppressLint("applySharedPref")
    public static void clearFilesDir(Context context) {
        File prefsDir = context.getFilesDir();
        PLog.d(TAG, "deleting data files");
        for (File file : prefsDir.listFiles()) {
            file.delete();
        }
    }
}
