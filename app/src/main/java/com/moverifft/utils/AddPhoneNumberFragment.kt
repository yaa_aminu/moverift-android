package com.moverifft.utils

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.users.data.UserManager
import com.moverifft.utils.PhoneNumberNormaliser.isValidPhoneNumber
import com.moverifft.utils.UiHelpers.showErrorDialog
import kotlinx.android.synthetic.main.fragment_add_phone_number.*
import kotlinx.android.synthetic.main.number_input.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class AddPhoneNumberFragment : BaseFragment() {

    override fun getLayout(): Int {
        return R.layout.fragment_add_phone_number
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_add_number.setOnClickListener {
            if (validate()) {
                addPhoneNumber(PhoneNumberNormaliser.toIEE(et_phone.text.trim().toString(), "GH"),
                        sp_mobile_money_type.selectedItem.toString())
            }
        }
        for (i in 0.until(number_input.childCount)) {
            number_input.getChildAt(i)
                    .setOnClickListener(onClicked)
        }
    }

    private val onClicked: (View) -> Unit = {
        when (it) {
            is TextView -> {
                et_phone.append(it.text)
            }
            is FrameLayout -> {
                if (et_phone.text.isNotEmpty()) {
                    val text = et_phone.text
                    text.delete(text.length - 1, text.length)
                }
            }
            else -> {
                throw AssertionError()
            }
        }
    }

    private fun validate(): Boolean {
        return if (sp_mobile_money_type.selectedItemPosition == 0) {
            showErrorDialog(context, getString(R.string.choose_wallet_type));false
        } else if (!isValidPhoneNumber(PhoneNumberNormaliser.toIEE(et_phone.text.trim().toString(), "GH"), "GH")) {
            showErrorDialog(context, getString(R.string.phone_number_invalid));false
        } else true

    }


    private fun addPhoneNumber(num: String, walletType: String) {
        val pdialog = UiHelpers.showProgressDialog(context)
        UserManager
                .getInstance()
                .sendVerification(num)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    pdialog.dismiss()
                    (activity as OnNumberAdded).onVerificationCodeSent(VerifiedNumber(num, walletType, System.currentTimeMillis()))
                }) {
                    pdialog.dismiss()
                    showErrorDialog(context, it.message)
                }
    }

}

interface OnNumberAdded {
    fun onVerificationCodeSent(num: VerifiedNumber)
}