package com.moverifft.utils

import android.app.ProgressDialog
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.users.data.UserManager
import kotlinx.android.synthetic.main.fragment_wallet_verification.*
import kotlinx.android.synthetic.main.number_input.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

const val NUM = "num"

class VerifyWalletNumberFragment : BaseFragment() {
    @Suppress("DEPRECATION")
    lateinit var dialog: ProgressDialog

    private var nextAttempt: Long = 0
    lateinit var num: VerifiedNumber
    lateinit var listener: VerificationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        num = arguments.getParcelable(NUM)
        listener = activity as VerificationListener
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        @Suppress("DEPRECATION")
        dialog = ProgressDialog(context)
        dialog.setCancelable(false)
        dialog.setMessage(getString(R.string.please_wait))
        for (i in 0.until(number_input.childCount)) {
            number_input.getChildAt(i)
                    .setOnClickListener(onClicked)
        }
        nextAttempt = UserManager.getInstance().nextAttempt
        updateRetry.run()
    }


    private val updateRetry = object : Runnable {
        override fun run() {
            if (!isDestroyed) {
                if (nextAttempt - System.currentTimeMillis() < 1000) {
                    tv_retry.text = getString(R.string.retry_sending)
                    tv_retry.setOnClickListener(retry)
                } else {
                    tv_retry.setOnClickListener(null)
                    tv_retry.text = getString(R.string.retry_in, summariseDelay(nextAttempt))
                    view?.postDelayed(this, 1000)
                }
            }
        }
    }

    private fun summariseDelay(nextAttempt: Long): String {
        val diff = nextAttempt - System.currentTimeMillis()
        val totalSeconds = diff / 1000
        val minutes = totalSeconds / 60
        val seconds = totalSeconds % 60
        return if (minutes >= 60) {
            String.format(Locale.US, "%02d:%02d:%02d", (minutes / 60).toInt(), minutes % 60, seconds)
        } else {
            String.format(Locale.US, "%02d:%02d", minutes, seconds)
        }
    }

    private val retry: (View) -> Unit = {
        dialog.show()

        UserManager.getInstance()
                .resendVerificationCode(num.num)
                .flatMap {
                    SystemClock.sleep(3000)
                    Observable.just(it)
                }
                .doOnError {
                    SystemClock.sleep(3000)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dialog.dismiss()
                    nextAttempt = UserManager.getInstance().nextAttempt
                    updateRetry.run()
                }, {
                    dialog.dismiss()
                    UiHelpers.showErrorDialog(context, it.message)
                })
    }
    private val onClicked: (View) -> Unit = {
        when (it) {
            is TextView -> {
                if (et_verification_code.text.length < 4) {
                    et_verification_code.append(it.text)
                }
            }
            is FrameLayout -> {
                if (et_verification_code.text.isNotEmpty()) {
                    val text = et_verification_code.text
                    text.delete(text.length - 1, text.length)
                }
            }
            else -> {
                throw AssertionError()
            }
        }
        if (et_verification_code.text.length >= 4) {
            verify()
        }
    }


    override fun getLayout(): Int {
        return R.layout.fragment_wallet_verification
    }

    private fun verify() {
        if (validate()) {
            dialog.show()
            UserManager.getInstance().verifyMobileMoneyNumber(
                    num.num!!, num.walletType!!, et_verification_code!!.text.toString().trim { it <= ' ' })
                    .doOnError {
                        SystemClock.sleep(3000)
                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread(), true)
                    .subscribe({ num ->
                        dialog.dismiss()
                        listener.onVerified(num)
                    }) { throwable ->
                        et_verification_code.setText("")
                        nextAttempt = UserManager.getInstance().nextAttempt
                        dialog.dismiss()
                        updateRetry.run()
                        GenericUtils.showDialog(context, throwable.message)
                    }
        }
    }

    private fun validate(): Boolean {
        if (et_verification_code!!.text.toString().trim { it <= ' ' }.isEmpty()) {
            et_verification_code!!.error = getString(R.string.phone_required)
            return false
        }
        return true
    }

    interface VerificationListener {
        fun onVerified(num: VerifiedNumber)
    }
}