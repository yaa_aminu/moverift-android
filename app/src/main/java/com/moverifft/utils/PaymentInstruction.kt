package com.moverifft.utils

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import com.moverifft.R
import com.moverifft.ui.common.BaseFragment
import com.moverifft.ui.driver.BaseActivity
import kotlinx.android.synthetic.main.fragment_bank_payment.*
import kotlinx.android.synthetic.main.fragment_payment_options.*


class PaymentInstruction : BaseActivity(), OnPaymentOptionClicked {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_instructions)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, PaymentOptionsFragment(), "main")
                .commit()
    }

    val files = arrayOf("bank-payment.html", "mobile-money.html", "cash-payment.html")
    override fun onPaymentOptionClicked(position: Int) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, getFragment(position)
                        .apply {
                            arguments = Bundle(1).apply { putString("file", files[position]) }
                        }, "$position.tag")
                .addToBackStack("back")
                .commit()
    }
}

@Suppress("NOTHING_TO_INLINE")
inline fun getFragment(pos: Int): BaseFragment {
    return when (pos) {
        0 -> BankPayment()
        1 -> MobileMoneyPayment()
        2 -> CashPayment()
        else -> throw AssertionError()
    }
}

class PaymentOptionsFragment : BaseFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_payment_options
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        payment_options.apply {
            adapter = ArrayAdapter<String>(context, R.layout.payment_option_list_item,
                    arrayOf(getString(R.string.bank_payment), getString(R.string.mobile_money_payment), getString(R.string.cash_payment)))
            setOnItemClickListener { _, _, pos, _ -> (activity as OnPaymentOptionClicked).onPaymentOptionClicked(pos) }
        }
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.payment)
    }
}

class MobileMoneyPayment : BaseFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_bank_payment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.add(0, R.id.action_add_wallet_number, 0, R.string.verify_wallet_number)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_add_wallet_number) {
            val intent = Intent(context, VerifiedNumbersActivity::class.java)
            intent.putExtra(com.moverifft.utils.ACTION, ADD_NUMBER)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.mobile_money_payment)
        web_view.loadUrl("file:///android_asset/docs/${arguments.getString("file")}")
    }
}

class CashPayment : BaseFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_bank_payment
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.cash_payment)
        web_view.loadUrl("file:///android_asset/docs/${arguments.getString("file")}")
    }
}

class BankPayment : BaseFragment() {
    override fun getLayout(): Int {
        return R.layout.fragment_bank_payment
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.bank_payment)
        web_view.loadUrl("file:///android_asset/docs/${arguments.getString("file")}")
    }
}

interface OnPaymentOptionClicked {
    fun onPaymentOptionClicked(position: Int)
}