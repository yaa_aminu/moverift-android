package com.moverifft.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.moverifft.R
import com.moverifft.ui.driver.BaseActivity
import kotlinx.android.synthetic.main.activity_open_source.*
import java.io.BufferedReader
import java.io.InputStreamReader


class OpenSourceActivity : BaseActivity() {


    var libraries = emptyList<Pair<String, String>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_source)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        loadList()
        list.setOnItemClickListener({ adapterView, _, position, _ ->
            val item = (adapterView.adapter as LibrariesAdapter).getItem(position)
            val intent = Intent(this, LicenceActivity::class.java)
            intent.putExtra(LicenceActivity.EXTRA_NAME, item.first)
            intent.putExtra(LicenceActivity.EXTRA_LICENSE, item.second)
            startActivity(intent)
        })
    }

    private fun loadList() {
        val dialog = UiHelpers.showProgressDialog(this)
        TaskManager.execute({
            val lists = ArrayList<Pair<String, String>>(50)
            BufferedReader(InputStreamReader(assets.open("open_source.txt"))).use {
                var line = it.readLine()
                while (line != null) {
                    val item = line.split("|")
                    lists.add(item[0] to item[1])
                    line = it.readLine()
                }
                SystemClock.sleep(2000)
            }
            runOnUiThread {
                libraries = lists
                list.adapter = LibrariesAdapter(this@OpenSourceActivity, libraries)
                dialog.dismiss()
            }

        }, false)
    }
}

class LibrariesAdapter(context: Context, items: List<Pair<String, String>>) : ArrayAdapter<Pair<String, String>>(context,
        android.R.layout.simple_list_item_1, items) {

    override fun getView(position: Int, tmp: View?, parent: ViewGroup?): View {
        val convertView: View = tmp ?: LayoutInflater.from(parent!!.context)
                .inflate(R.layout.settings_list_item, parent, false)
        (convertView as TextView).text = getItem(position).first
        return convertView
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return getView(position, convertView, parent)
    }
}