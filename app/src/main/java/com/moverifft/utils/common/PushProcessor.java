package com.moverifft.utils.common;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.moverifft.BuildConfig;
import com.moverifft.MoveRifft;
import com.moverifft.R;
import com.moverifft.ui.clients.LatLang;
import com.moverifft.ui.clients.Trip;
import com.moverifft.ui.clients.TripDetailsActivity;
import com.moverifft.ui.clients.TripsActivity;
import com.moverifft.ui.clients.TripsApi;
import com.moverifft.ui.driver.NotificationEntry;
import com.moverifft.utils.FileUtils;
import com.moverifft.utils.GenericUtils;
import com.moverifft.utils.PLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;

import javax.inject.Inject;

import io.realm.Realm;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yaaminu on 8/23/17.
 */

public class PushProcessor extends IntentService {
    public static final String EXTRA_DATA = "data.extra";
    private static final String TAG = "PushProcessor";
    @Inject
    TripsApi tripsApi;

    public PushProcessor() {
        super(TAG);
        MoveRifft.depGraph.inject(this);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent serviceIntent) {
        GenericUtils.assertThat(serviceIntent != null);
        final String pushData = serviceIntent.getStringExtra(EXTRA_DATA);
        // TODO: 8/26/17 use a job to process the intent
        PLog.d(TAG, pushData);
        try {
            JSONObject jsonObject = new JSONObject(pushData).getJSONObject("message");
            String action = jsonObject.getString("action");
            if ("acceptTrip".equals(action)) {
                String endLocation = jsonObject.getString("endLocation"),
                        driverDp = jsonObject.getString("driverDp"),
                        vehicleNo = jsonObject.getString("vehicleNo"),
                        driverName = jsonObject.getString("driverName"),
                        goodsType = jsonObject.getString("goodsType");

                addNotificationEntry(jsonObject.getString("tripId") + ".accept", getString(R.string.driver_started_trip, driverName),
                        getString(R.string.moving_load_template, goodsType, LatLang.fromJson(endLocation).getName()),
                        driverDp, vehicleNo,
                        NotificationEntry.Companion.getTYPE_TRIP_STARTED(),
                        new JSONObject().put("tripId", jsonObject.getString("tripId")));
                notifyUser(jsonObject.getString("tripId"), "Trip started");
            } else if ("endTrip".equals(action)) {
                // TODO: 8/24/17 play ringtone
                // TODO: 8/24/17 vibrate
                String endLocation = jsonObject.getString("endLocation"),
                        driverDp = jsonObject.getString("driverDp"),
                        vehicleNo = jsonObject.getString("vehicleNo"),
                        driverName = jsonObject.getString("driverName"),
                        goodsType = jsonObject.getString("goodsType");

                String tripId = jsonObject.getString("tripId");
                addNotificationEntry(tripId + ".accept", getString(R.string.driver_ended_trip, driverName),
                        getString(R.string.moving_load_template, goodsType, LatLang.fromJson(endLocation).getName()),
                        driverDp, vehicleNo,
                        NotificationEntry.Companion.getTYPE_TRIP_ENDED(), new JSONObject()
                                .put("tripId", tripId));
                notifyUser(tripId, "Trip Complete");
            } else {
                PLog.d(TAG, "unknown push type");
                if (BuildConfig.DEBUG) {
                    throw new RuntimeException("unknown action: " + action);
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void addNotificationEntry(@Nullable String id, String title, String details, String dp, String vehicleNo, int type, JSONObject data) {
        if (id == null) {
            byte[] bytes = new byte[32];
            new SecureRandom().nextBytes(bytes);
            id = FileUtils.sha1(bytes);
            id = FileUtils.sha1(id + System.currentTimeMillis() + type);
        }
        final NotificationEntry notificationEntry = new NotificationEntry(
                id, title, type, System.currentTimeMillis(), details, dp, vehicleNo, data.toString()
        );
        Realm realm = Realm.getDefaultInstance();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(notificationEntry);
                }
            });
        } finally {
            realm.close();
        }
    }


    private void notifyUser(final String tripId, final String message) {
        tripsApi.findTripById(tripId)
                .subscribeOn(Schedulers.immediate())
                .observeOn(Schedulers.immediate())
                .subscribe(new Action1<Trip>() {
                    @Override
                    public void call(Trip trip) {
                        Intent intent = new Intent(PushProcessor.this, TripDetailsActivity.class);
                        intent.putExtra(TripDetailsActivity.EXTRA_TRIP, trip);
                        doNotify(PendingIntent.getActivity(PushProcessor.this,
                                3000, intent, PendingIntent.FLAG_UPDATE_CURRENT), tripId, message);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        PLog.e(TAG, throwable.getMessage(), throwable);
                        doNotify(PendingIntent.getActivity(PushProcessor.this,
                                3000, new Intent(PushProcessor.this,
                                        TripsActivity.class), PendingIntent.FLAG_UPDATE_CURRENT), tripId, message);
                    }
                });

    }

    private void doNotify(@Nullable PendingIntent pendingIntent, String tripId, String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel")
                .setContentTitle(message)
                .setSound(AppSettings.getSetting(AppSettings.NOTIFICATION_SOUND))
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true);
        if (pendingIntent != null) {
            builder.setContentIntent(pendingIntent);
        }
        NotificationManagerCompat.from(this).notify(tripId, 1200, builder.build());
    }
}
