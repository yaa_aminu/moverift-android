package com.moverifft.utils.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.moverifft.R;
import com.moverifft.ui.common.BaseFragment;
import com.moverifft.utils.GenericUtils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by yaaminu on 8/24/17.
 */

public class ClientDetailFragment extends BaseFragment {

    public static final String ARG_DISTANCE_APART = "distanceApart";
    public static final String ARG_CLIENT_NAME = "clientName";
    public static final String ARG_USER_AVARTAR = "arg_user_avartar";

    @BindView(R.id.client_name)
    TextView client_name;
    @BindView(R.id.tv_distance_away)
    TextView tv_distance_away;
    @BindView(R.id.user_avatar)
    ImageView avatar;


    @Override
    protected int getLayout() {
        return R.layout.client_detail_fragment;
    }


    public static ClientDetailFragment create(double distanceApart, @NonNull String userName, @NonNull String dp
    ) {
        Bundle bundle = new Bundle(3);
        bundle.putDouble(ARG_DISTANCE_APART, distanceApart);
        bundle.putString(ARG_CLIENT_NAME, userName);
        bundle.putString(ARG_USER_AVARTAR, dp);
        ClientDetailFragment clientDetailFragment = new ClientDetailFragment();
        clientDetailFragment.setArguments(bundle);
        return clientDetailFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        double distanceAway = getArguments().getDouble(ARG_DISTANCE_APART);
        String clientName = getArguments().getString(ARG_CLIENT_NAME);
        String userAvartar = getArguments().getString(ARG_USER_AVARTAR);
        client_name.setText(clientName);
        tv_distance_away.setText(normaliseDistance(distanceAway));
        Picasso.with(getContext())
                .load(userAvartar)
                .placeholder(R.drawable.ic_person_white_24dp)
                .error(R.drawable.ic_person_white_24dp)
                .into(avatar);
    }


    private String normaliseDistance(double distance) {
        if (distance > 1000) {
            return getString(R.string.distance_apart,
                    GenericUtils.formatDistance(distance / 1000) + " Km");
        }
        return getString(R.string.distance_apart, GenericUtils.formatDistance(distance) + " metres");
    }
}
