package com.moverifft.utils.common;

import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by yaaminu on 8/31/17.
 */

public class AppSettings {

    public static final String NOTIFICATION_SOUND = "settings.notification.new_message.sound",
            INCOMING_TRIP_REQUEST_SOUND = "settings.notification.incoming_trip_request";

    @Nullable
    public static Uri getSetting(@NonNull String key) {
        switch (key) {
            case NOTIFICATION_SOUND:
                return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            case INCOMING_TRIP_REQUEST_SOUND:
                return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            default:
                throw new AssertionError();
        }
    }
}
