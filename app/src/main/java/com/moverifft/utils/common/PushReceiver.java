package com.moverifft.utils.common;

import android.content.Context;
import android.content.Intent;

import com.moverifft.utils.PLog;
import com.parse.ParsePushBroadcastReceiver;

/**
 * Created by yaaminu on 8/23/17.
 */

public class PushReceiver extends ParsePushBroadcastReceiver {
    private static final String TAG = "PushReceiver";

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        String pushDataStr = intent.getStringExtra(KEY_PUSH_DATA);
        PLog.d(TAG, "push received %s", pushDataStr);
        intent = new Intent(context, PushProcessor.class);
        intent.putExtra(PushProcessor.EXTRA_DATA, pushDataStr);
        context.startService(intent);
    }
}
