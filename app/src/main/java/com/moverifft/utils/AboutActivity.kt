package com.moverifft.utils

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.moverifft.BuildConfig
import com.moverifft.R
import com.moverifft.ui.driver.BaseActivity
import kotlinx.android.synthetic.main.activity_about.*


class AboutActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        open_source.setOnClickListener {
            startActivity(Intent(this, OpenSourceActivity::class.java))
        }
        terms.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://cliqhaul.com/legal/terms")
            startActivity(intent)
        }
        privacy.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://cliqhaul.com/legal/privacy")
            startActivity(intent)
        }
        version_name.text = getString(R.string.version, BuildConfig.VERSION_NAME)
    }
}