package com.moverifft.utils

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.moverifft.R
import com.moverifft.ui.common.LauncherActivity
import com.moverifft.ui.driver.BaseActivity
import com.moverifft.users.data.UserManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_settings.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class SettingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val user = UserManager.getInstance()
                .getCurrentUser()!!

        tv_user_name.text = user.name
        tv_email.text = user.email
        tv_phone.text = PhoneNumberNormaliser.toLocalFormat(user.phoneNumber, "GH")
        Picasso.with(this)
                .load(user.dp)
                .placeholder(R.drawable.ic_person_white_200dp)
                .error(R.drawable.ic_person_white_200dp)
                .into(user_avatar)

        log_out.setOnClickListener {
            confirmAndLogOut()
        }
        tv_verified_numbers.setOnClickListener {
            startActivity(Intent(this, VerifiedNumbersActivity::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings_menu, menu!!)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_about -> startActivity(Intent(this, AboutActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun confirmAndLogOut() {
        GenericUtils.showComfirmationDialog(this, getString(R.string.logout_warning)) {
            val dialog = UiHelpers.showProgressDialog(this)
            UserManager.getInstance()
                    .logout()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        dialog.dismiss()
                        val intent = Intent(this, LauncherActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        finish()
                    }) {
                        dialog.dismiss()
                        UiHelpers.showErrorDialog(this, it.message)
                    }
        }
    }
}