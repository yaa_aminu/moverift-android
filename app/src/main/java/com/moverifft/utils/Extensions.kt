package com.moverifft.utils

import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.Sort

/**
 * Created by yaaminu on 1/18/18.
 */


inline fun <reified T : RealmObject> Realm.findAsync(fieldName: String = "", order: Sort = Sort.ASCENDING): RealmResults<T> {
    val query = where(T::class.java)
    return if (fieldName.isNotEmpty()) {
        query.findAllSortedAsync(fieldName, order)
    } else {
        query.findAllAsync()
    }
}