package com.moverifft.utils

import android.os.Bundle
import android.os.SystemClock
import com.moverifft.R
import com.moverifft.ui.driver.BaseActivity
import kotlinx.android.synthetic.main.activity_licence.*
import org.apache.commons.io.IOUtils


class LicenceActivity : BaseActivity() {

    companion object {
        const val EXTRA_NAME = "name"
        const val EXTRA_LICENSE = "license"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_licence)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        library_name.text = intent.getStringExtra(EXTRA_NAME)!!
        val license = intent.getStringExtra(EXTRA_LICENSE)!!
        TaskManager.execute({
            assets.open("license/$license.txt").use {
                val txt = IOUtils.toString(it)
                runOnUiThread {
                    licence_text.text = txt
                }
                SystemClock.sleep(1500)
            }
        }, false)
    }
}