package com.moverifft.utils

import android.os.Parcel
import android.os.Parcelable
import com.parse.ParseObject
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class VerifiedNumber(@PrimaryKey var num: String?,
                          var walletType: String?, private var updatedAt: Long) : RealmObject(), Parcelable {

    var localNum: String? = PhoneNumberNormaliser.toLocalFormat(num, "GH")

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readLong())

    //required by realm
    constructor() : this("", "", 0L)

    constructor(obj: ParseObject) : this(obj.getString("number"), obj.getString("type"), obj.updatedAt.time)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(num)
        parcel.writeString(walletType)
        parcel.writeLong(updatedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VerifiedNumber> {
        override fun createFromParcel(parcel: Parcel): VerifiedNumber {
            return VerifiedNumber(parcel)
        }

        override fun newArray(size: Int): Array<VerifiedNumber?> {
            return arrayOfNulls(size)
        }
    }

}