package com.moverifft

import butterknife.ButterKnife
import com.moverifft.users.data.UserManager
import io.realm.Realm
import io.realm.RealmConfiguration
import org.jetbrains.anko.doAsync
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by yaaminu on 6/8/17.
 */
class MoveRifft : android.app.Application() {

    companion object {
        lateinit var depGraph: MoveRifftGraph
    }

    @javax.inject.Inject
    lateinit var jobManager: com.birbit.android.jobqueue.JobManager

    override fun onCreate() {
        super.onCreate()
        ButterKnife.setDebug(BuildConfig.DEBUG)
        Realm.init(this)
        val configBuilder = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(configBuilder)
        com.moverifft.utils.PLog.setLogLevel(if (BuildConfig.DEBUG) com.moverifft.utils.PLog.LEVEL_VERBOSE else com.moverifft.utils.PLog.LEVEL_ERROR)
        com.moverifft.utils.Config.init(this)
        UserManager.initialize(this)
        com.moverifft.MoveRifft.Companion.depGraph = DaggerMoveRifftGraph.builder()
                .moveRiftAppModule(MoveRiftAppModule(this))
                .build()
        com.moverifft.MoveRifft.Companion.depGraph.inject(this)
        com.moverifft.utils.TaskManager.init(jobRunner)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath).build())
    }

    val jobRunner = object : com.moverifft.utils.TaskManager.JobRunner {
        override fun runJobBlocking(task: com.moverifft.utils.Task): String {
            jobManager.addJob(task)
            return task.id
        }

        override fun cancelJobsAsync(tag: String) {
            doAsync {
                jobManager.cancelJobs(com.birbit.android.jobqueue.TagConstraint.ALL, tag)
            }
        }

        override fun startAsync() {
            doAsync {
                jobManager.start()
            }
        }
    }
}