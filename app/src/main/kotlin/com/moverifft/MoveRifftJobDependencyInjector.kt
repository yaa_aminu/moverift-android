package com.moverifft

/**
 * Created by yaaminu on 6/8/17.
 */

class MoveRiftJobDependencyInjector : com.birbit.android.jobqueue.di.DependencyInjector {
    companion object {
        const val TAG = "MoveRiftJobDependencyIn"
    }

    override fun inject(job: com.birbit.android.jobqueue.Job) {
        com.moverifft.utils.PLog.w(com.moverifft.MoveRiftJobDependencyInjector.Companion.TAG, "injecting job: $job")
    }
}