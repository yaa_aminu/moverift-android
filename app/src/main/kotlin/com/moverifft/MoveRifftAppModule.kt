package com.moverifft

import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import com.birbit.android.jobqueue.di.DependencyInjector
import com.moverifft.utils.PLog
import com.moverifft.utils.Task

import dagger.Module
import dagger.Provides

/**
 * Created by yaaminu on 6/8/17.
 */
@Module
class MoveRiftAppModule(private val moveRifft: MoveRifft) {

    @Provides
    fun manager(configuration: Configuration): JobManager {
        return JobManager(configuration)
    }

    @Provides
    fun getConfiguration(injector: DependencyInjector): Configuration {
        return Configuration.Builder(moveRifft)
                .jobSerializer(Task.JobSerializer())
                .injector(injector)
                .customLogger(PLog(JobManager::class.java.name))
                .build()
    }

    @Provides
    fun injector(): DependencyInjector {
        return MoveRiftJobDependencyInjector()
    }
}
