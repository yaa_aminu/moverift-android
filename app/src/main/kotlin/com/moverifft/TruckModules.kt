package com.moverifft

import com.moverifft.truck.data.TruckApi
import com.moverifft.truck.data.TruckManager
import com.moverifft.users.net.ParseBackend
import dagger.Module
import dagger.Provides

/**
 * Created by yaaminu on 6/13/17.
 */
@Module
class TruckModules {
    @Provides
    fun getTruckApi(backend: ParseBackend): TruckApi {
        return TruckManager(backend)
    }
}