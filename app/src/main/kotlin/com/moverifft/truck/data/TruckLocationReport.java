package com.moverifft.truck.data;

import android.support.annotation.NonNull;

import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import static com.moverifft.common.Constants.LAT_LANG;
import static com.moverifft.common.Constants.TIME_REPORTED;

/**
 * Created by yaaminu on 8/19/17.
 */

public class TruckLocationReport {

    private final long timeReported;
    private final double latitude;
    private final double longitude;
    private String truckId;

    public TruckLocationReport(String truckId,
                               long timeReported,
                               double latitude, double longitude) {
        this.truckId = truckId;
        this.timeReported = timeReported;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Nullable
    public static TruckLocationReport create(@Nullable ParseObject parseObject) {
        if (parseObject == null) {
            return null;
        }
        final ParseGeoPoint geoPoint = parseObject.getParseGeoPoint(LAT_LANG);
        return new TruckLocationReport(parseObject.getObjectId(),
                //sometimes we don' fetch the time reported for efficiency reasons
                parseObject.isDataAvailable(TIME_REPORTED) ? parseObject.getLong(TIME_REPORTED) : 0L,
                geoPoint.getLatitude(), geoPoint.getLongitude());
    }


    public long getTimeReported() {
        return timeReported;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTruckId() {
        return truckId;
    }


    @Override
    public String toString() {
        return "TruckLocationReport{" +
                ", timeReported=" + timeReported +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", truckId='" + truckId + '\'' +
                '}';
    }


    @NonNull
    public JSONObject toJson() {
        try {
            return new JSONObject()
                    .put("lat", latitude)
                    .put("lng", longitude)
                    .put("timeReported", timeReported)
                    .put("truckId", truckId);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    public static TruckLocationReport fromJson(@NonNull String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return new TruckLocationReport(jsonObject.getString("truckId"),
                    jsonObject.getLong("timeReported"), jsonObject.getDouble("lat"),
                    jsonObject.getDouble("lng"));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
