package com.moverifft.truck.data

import android.annotation.SuppressLint
import com.moverifft.common.Constants
import com.moverifft.ui.clients.LatLang
import com.moverifft.users.data.User
import com.moverifft.users.data.UserManager
import com.moverifft.users.net.ParseBackend
import com.moverifft.utils.Config
import com.moverifft.utils.PLog
import com.parse.ParseGeoPoint
import com.parse.ParseObject
import rx.Observable
import rx.schedulers.Schedulers

/**
 * Created by yaaminu on 6/13/17.
 */
class TruckManager(private val backend: ParseBackend) : TruckApi {
    val TAG: String = TruckManager::class.java.simpleName

    @SuppressLint("ApplySharedPref")
    override fun updateTrackType(truckType: Int): Observable<Truck> {
        return backend.updateTruckType(truckType)
                .map {
                    Config.getApplicationWidePrefs()
                            .edit()
                            .putBoolean(UserManager.KEY_SETUP, true)
                            .commit()
                    create(it)
                }//todo persist truck
    }

    override fun markAvailable(truck: Truck, available: Boolean): Observable<Truck> {
        return backend.markAvailable(truck.truckId, available)
    }

    override fun addTruckForCurrentUser(vehicleNo: String): Observable<Truck> {
        //TODO persist the data to realm
        return backend.addTruck(vehicleNo)
    }

    override fun getCurrentUserTruck(user: User): Observable<Truck> {
        @Suppress("UsePropertyAccessSyntax")
        return backend.getCurrentUserTruck()
                .map({ create(it) })
    }


    override fun reportLocation(report: TruckLocationReport) {
        backend.reportLocation(report)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe({
                    PLog.d(TAG, "reported location %s", it)
                }, {
                    PLog.e(TAG, it.message, it)
                })
    }


    override fun findAvailableTrucks(truckType: Int): Observable<List<TruckLocationReport>> {
        return backend.findAvailableTrucksLocations(truckType)
    }

    override fun findTruckById(truckId: String): Observable<Truck> {
        return backend.findTruckById(truckId)
    }

    companion object {
        @JvmStatic
        fun create(parseObject: ParseObject?): Truck? {
            if (parseObject == null) {
                return null
            }
            val parseGeoPoint: ParseGeoPoint?
                    = parseObject.getParseGeoPoint(Constants.LAT_LANG)
            return Truck(
                    parseObject.getString(Constants.OWNER_ID),
                    parseObject.getString(Constants.OWNER_NAME),
                    parseObject.getInt(Constants.RATING),
                    parseObject.getInt(Constants.TRUCK_TYPE_IDENTIFIER),
                    parseObject.getString(Constants.REG_NO),
                    parseObject.objectId,
                    parseObject.getBoolean(Constants.AVAILABLE),
                    parseObject.getBoolean(Constants.TRUCK_VERIFIED),
                    LatLang.from(parseGeoPoint), parseObject.getString("ownerDp"))
        }
    }


}