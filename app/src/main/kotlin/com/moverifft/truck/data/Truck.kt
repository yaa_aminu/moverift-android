package com.moverifft.truck.data

import android.os.Parcel
import android.os.Parcelable
import com.moverifft.ui.clients.LatLang

/**
 * Created by yaaminu on 6/13/17.
 */
data class Truck(val ownerId: String, val ownerName: String, val rating: Int, val truckTypeIdentifier: Int,
                 val regNo: String, val truckId: String, val available: Boolean,
                 val verified: Boolean, val currentLocation: LatLang?, val ownerDp: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readByte() != 0.toByte(),
            parcel.readParcelable(LatLang::class.java.classLoader),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ownerId)
        parcel.writeString(ownerName)
        parcel.writeInt(rating)
        parcel.writeInt(truckTypeIdentifier)
        parcel.writeString(regNo)
        parcel.writeString(truckId)
        parcel.writeByte(if (available) 1 else 0)
        parcel.writeByte(if (verified) 1 else 0)
        parcel.writeParcelable(currentLocation, flags)
        parcel.writeString(ownerDp)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Truck> {
        override fun createFromParcel(parcel: Parcel): Truck {
            return Truck(parcel)
        }

        override fun newArray(size: Int): Array<Truck?> {
            return arrayOfNulls(size)
        }
    }
}