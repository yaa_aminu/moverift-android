package com.moverifft.truck.data;

/**
 * Created by yaaminu on 8/21/17.
 */

public interface TruckLocationChangesListener {
    void onTruckLocationUpdate(TruckLocationReport report);
}
