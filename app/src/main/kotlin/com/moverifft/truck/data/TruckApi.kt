package com.moverifft.truck.data

import com.moverifft.users.data.User
import rx.Observable

/**
 * Created by yaaminu on 6/13/17.
 */
interface TruckApi {
    fun addTruckForCurrentUser(vehicleNo: String): Observable<Truck>
    fun updateTrackType(truckType: Int): Observable<Truck>
    fun markAvailable(truck: Truck, available: Boolean): Observable<Truck>
    fun getCurrentUserTruck(user: User): Observable<Truck>
    fun reportLocation(report: TruckLocationReport)
    fun findAvailableTrucks(truckType: Int): Observable<List<TruckLocationReport>>
    fun findTruckById(truckId: String): Observable<Truck>
}