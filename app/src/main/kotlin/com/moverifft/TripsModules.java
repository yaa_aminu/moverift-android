package com.moverifft;

import com.moverifft.ui.clients.TripsApi;
import com.moverifft.ui.clients.TripsManager;
import com.moverifft.users.net.ParseBackend;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yaaminu on 8/22/17.
 */

@Module
public class TripsModules {
    @Provides
    public TripsApi getTripsApi(ParseBackend parseBackend) {
        return new TripsManager(parseBackend);
    }
}
