package com.moverifft.users.data

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import com.moverifft.R
import com.moverifft.ui.common.RecyclerViewBaseAdapter

/**
 * Created by yaaminu on 1/8/18.
 */


internal class TruckTypeAdapter(delegate: RecyclerViewBaseAdapter.Delegate<TruckType>) :
        com.moverifft.ui.common.RecyclerViewBaseAdapter<TruckType, TruckTypeHolder>(delegate) {

    override fun doBindHolder(holder: TruckTypeHolder, position: Int) {
        holder.tv_truck_type_name.text = getItem(position).title
        holder.iv_truck_type.setImageResource(getItem(position).icon)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TruckTypeHolder {
        return TruckTypeHolder(inflater.inflate(R.layout.menu_truck_type_list_item, parent, false))
    }
}

internal class TruckTypeHolder(view: View) : RecyclerViewBaseAdapter.Holder(view) {

    @BindView(R.id.tv_truck_type_name)
    lateinit var tv_truck_type_name: TextView
    @BindView(R.id.truck_type_icon)
    lateinit var iv_truck_type: ImageView

}