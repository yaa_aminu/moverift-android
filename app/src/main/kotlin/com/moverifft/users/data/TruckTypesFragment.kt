package com.moverifft.users.data

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import com.moverifft.R
import com.moverifft.truck.data.Truck
import com.moverifft.truck.data.TruckApi
import com.moverifft.truck.data.TruckManager
import com.moverifft.ui.clients.PagerAdapter
import com.moverifft.ui.clients.TruckTypeFragmentSection
import com.moverifft.ui.common.BaseFragment
import com.moverifft.ui.driver.LoginActivity
import com.moverifft.users.net.ParseBackend
import com.moverifft.utils.GenericUtils
import kotlinx.android.synthetic.main.choose_truck_type_fragment.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by yaaminu on 8/17/17.
 */

class TruckTypesFragment : BaseFragment(), TruckTypeFragmentSection.OnTruckTypeSelected {
    override fun onTruckTypeSelected(type: Int) {
        updateTruckAndProceed(type)
    }

    internal var userApi: UserApi? = null
    internal var truckApi: TruckApi? = null
    private var progressDialog: ProgressDialog? = null

    private val observer = object : rx.Observer<Truck> {
        override fun onCompleted() {
            progressDialog!!.dismiss()
            (activity as LoginActivity)
                    .onCompleted()
        }

        override fun onError(e: Throwable) {
            progressDialog!!.dismiss()
            GenericUtils.showDialog(context, e.message)
        }

        override fun onNext(truck: Truck) {

        }
    }

    override fun getLayout(): Int {
        return R.layout.choose_truck_type_fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userApi = UserManager.getInstance()
        truckApi = TruckManager(ParseBackend.getInstance())
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(getString(R.string.completing_registration))
        progressDialog!!.setCancelable(false)
        val pagerAdapter = PagerAdapter(childFragmentManager)
        pager.adapter = pagerAdapter
        tab_layout.setupWithViewPager(pager)
    }

    private fun updateTruckAndProceed(type: Int) {
        progressDialog!!.show()
        truckApi!!.updateTrackType(type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer)
    }
}
