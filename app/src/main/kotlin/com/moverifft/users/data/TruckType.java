package com.moverifft.users.data;

import android.support.annotation.DrawableRes;

/**
 * Created by yaaminu on 8/17/17.
 */

public class TruckType {
    public final int truckId;
    public final String title;
    @DrawableRes
    public final int icon;

    public TruckType(String title, int truckId, @DrawableRes int icon) {
        this.title = title;
        this.icon = icon;
        this.truckId = truckId;
    }
}
