package com.moverifft.users.data

import com.moverifft.common.Constants
import com.moverifft.common.Constants.IS_VERIFIED
import com.moverifft.common.Constants.PHONE_NUMBER
import com.moverifft.utils.Config
import com.parse.ParseUser

/**
 * Created by yaaminu on 6/8/17.
 */
data class User(val name: String,
                val userId: String,
                val phoneNumber: String,
                val isVerified: Boolean = false,
                val email: String,
                val truckId: String?, val dp: String?) {

    companion object {
        @JvmStatic
        fun create(parseUser: ParseUser?): User? {
            if (parseUser == null) {
                return null
            }
            var dp = parseUser.getString("dp")
            if (dp == null) {
                dp = Config.getApplicationWidePrefs()
                        .getString("current.user.dp.local", null)
            }
            return User(parseUser.getString(Constants.NAME), parseUser.objectId, parseUser.getString(PHONE_NUMBER),
                    parseUser.getBoolean(IS_VERIFIED), parseUser.getString(Constants.EMAIL_ADDRESS),
                    parseUser.getString(Constants.TRUCK_ID), dp)
        }

    }
}