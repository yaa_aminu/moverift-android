package com.moverifft.users.data

import com.moverifft.utils.VerifiedNumber
import rx.Observable

/**
 * Created by yaaminu on 6/8/17.
 */
interface UserApi {
    fun login(name: String, phoneNumber: String, email: String): rx.Observable<User>
    fun verify(verificationCode: String): rx.Observable<User>
    fun getCurrentUser(): User?
    fun isCurrentUserVerified(): Boolean
    fun isCurrentUserSetup(): Boolean
    fun isUserLoggedIn(): Boolean
    fun logout(): rx.Observable<Any>
    fun getCurrentUserTruckId(): String?
    fun addNameMapping(userId: String, name: String)
    fun getNameFromId(userId: String): String?
    fun getUserDpNonBlocking(userId: String): String?
    fun getUserDp(userId: String): Observable<String>
    fun getStage(): Int
    fun verifyMobileMoneyNumber(num: String, walletType: String, code: String): Observable<VerifiedNumber>
    fun sendVerification(num: String): Observable<String>
    fun loadVerifiedNumbers()
}