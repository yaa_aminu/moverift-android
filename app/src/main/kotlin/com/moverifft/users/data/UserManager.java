package com.moverifft.users.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.moverifft.BuildConfig;
import com.moverifft.MoveRifft;
import com.moverifft.truck.data.TruckApi;
import com.moverifft.ui.common.Callback;
import com.moverifft.users.net.ParseBackend;
import com.moverifft.utils.Config;
import com.moverifft.utils.GenericUtils;
import com.moverifft.utils.PLog;
import com.moverifft.utils.TaskManager;
import com.moverifft.utils.VerifiedNumber;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.realm.Realm;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by yaaminu on 8/17/17.
 */

public class UserManager implements UserApi {

    private static final String TAG = "UserManager";

    public static final String KEY_SETUP = "setup";
    private final ParseBackend backend;

    @Inject
    TruckApi truckApi;

    public UserManager(ParseBackend backend) {
        this.backend = backend;
        MoveRifft.depGraph.inject(this);
    }

    @NotNull
    @Override
    public Observable<User> login(@NotNull String name, @NotNull String phoneNumber,
                                  @NonNull String email) {

        return backend.loginOrSignup(name, phoneNumber, email)
                .map(new Func1<ParseUser, User>() {
                    @Override
                    public User call(ParseUser parseObject) {
                        return User.create(parseObject);
                    }
                });
    }

    @NotNull
    @Override
    public Observable<User> verify(@NotNull String verificationCode) {
        return backend.verify(verificationCode)
                .map(new Func1<ParseUser, User>() {
                    @Override
                    public User call(ParseUser parseObject) {
                        clearVerificationRetryPrefs();
                        loadVerifiedNumbers();
                        return User.create(parseObject);
                    }
                });
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return User.create(backend.getCurrentUser());
    }

    @Override
    public boolean isCurrentUserVerified() {
        User user = getCurrentUser();
        return user != null && user.isVerified();
    }

    @Override
    public boolean isCurrentUserSetup() {
        if (!isCurrentUserVerified()) {
            return false;
        }
        final User currentUser = getCurrentUser();
        GenericUtils.assertThat(currentUser != null, "must not be null");
        //noinspection SimplifiableIfStatement
        if (currentUser.getDp() == null && !hasSkippedDp()) return false;
        return true;
    }

    @Override
    public int getStage() {
        if (getCurrentUser() == null) {
            return 1;
        } else if (!isCurrentUserVerified()) {
            return 2;
        } else if (getCurrentUser().getDp() == null && !hasSkippedDp()) {
            return 3;
        } else {
            return 4;
        }
    }

    @Override
    public boolean isUserLoggedIn() {
        return getCurrentUser() != null;
    }

    @NotNull
    @Override
    public Observable<Object> logout() {
        return backend.logout()
                .map(new Func1<Object, Object>() {
                    @Override
                    public Object call(Object o) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.deleteAll();
                        realm.commitTransaction();
                        GenericUtils.clearSharedPrefs(Config.getApplicationContext());
                        GenericUtils.clearFilesDir(Config.getApplicationContext());
                        return true;
                    }
                });
    }

    @Nullable
    @Override
    public String getCurrentUserTruckId() {
        final User currentUser = getCurrentUser();
        return currentUser != null ? currentUser.getTruckId() : null;
    }

    @Override
    public void addNameMapping(@NotNull String userId, @NotNull String name) {
        backend.addMapping(userId, name);
    }

    @NotNull
    @Override
    public Observable<String> getUserDp(@NotNull String userId) {
        return backend.getUserDp(userId);
    }

    @Nullable
    @Override
    public String getNameFromId(@NotNull String userId) {
        return backend.getNameFromId(userId);
    }

    @Nullable
    @Override
    public String getUserDpNonBlocking(@NotNull String userId) {
        return backend.getUserDpNonBlocking(userId);
    }

    @NotNull
    @Override
    public Observable<VerifiedNumber> verifyMobileMoneyNumber(@NotNull String num, @NotNull String walletType, @NotNull String verificationCode) {
        return backend.verifyPaymentWallet(num, walletType, verificationCode)
                .map(new Func1<VerifiedNumber, VerifiedNumber>() {
                    @Override
                    public VerifiedNumber call(VerifiedNumber verifiedNumber) {
                        clearVerificationRetryPrefs();
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(verifiedNumber);
                        realm.commitTransaction();
                        return verifiedNumber;
                    }
                });
    }

    @NotNull
    @Override
    public Observable<String> sendVerification(@NotNull final String num) {
        return Observable.from(TaskManager.execute(new Callable<String>() {
            @Override
            public String call() throws Exception {
                backend.sendVerificationCode(num);
                return num;
            }
        }, true));
    }

    public void updateCurrentUserDp(String path, Callback<Boolean> cb) {
        backend.updateCurrentUserDp(path, cb);
    }

    public static void initialize(Context context) {
        com.parse.Parse.initialize(new com.parse.Parse.Configuration.Builder(context)
                .server(BuildConfig.SERVER_URL)
                .enableLocalDataStore()
                .applicationId(BuildConfig.APP_ID)
                .build());
        if (ParseUser.getCurrentUser() != null) {
            ParseInstallation.getCurrentInstallation().saveInBackground();
        }
        com.parse.Parse.setLogLevel(com.parse.Parse.LOG_LEVEL_VERBOSE);
    }

    public void skipDp() {
        Config.getPreferences("session")
                .edit()
                .putBoolean("user.dp.skip", true)
                .apply();
    }

    private boolean hasSkippedDp() {
        return Config.getPreferences("session")
                .getBoolean("user.dp.skip", false);
    }

    public Observable<Object> resendVerificationCode() {
        return backend.resendVerificationCode()
                .map(new Func1<Object, Object>() {
                    @Override
                    public Object call(Object o) {
                        updateNextAttempt();
                        return Observable.just(o);
                    }
                });
    }

    public Observable<Object> resendVerificationCode(String number) {
        return backend.resendVerificationCode(number)
                .map(new Func1<Object, Object>() {
                    @Override
                    public Object call(Object o) {
                        updateNextAttempt();
                        return Observable.just(o);
                    }
                });
    }

    private void updateNextAttempt() {
        Config.getPreferences("session")
                .edit()
                .putLong("verification.delay.next.attempt", System.currentTimeMillis() + getDelay())
                .apply();
    }

    private void clearVerificationRetryPrefs() {
        Config.getPreferences("session")
                .edit()
                .remove("verification.delay.next.attempt")
                .remove("verification.delay.seconds")
                .apply();
    }

    private long getDelay() {
        int currentDelay = Config.getPreferences("session")
                .getInt("verification.delay.seconds", 0);
        if (currentDelay < 30) {
            currentDelay = 30;
        } else if (currentDelay < 60 * 3) {
            currentDelay = 60 * 3;
        } else if (currentDelay < 60 * 15) {
            currentDelay = 60 * 15;
        } else if (currentDelay < 60 * 30) {
            currentDelay = 60 * 30;
        } else {
            currentDelay = 60 * 60;
        }
        Config.getPreferences("session")
                .edit().putInt("verification.delay.seconds", currentDelay).apply();
        return SECONDS.toMillis(currentDelay);
    }

    public long getNextAttempt() {
        return Config.getPreferences("session")
                .getLong("verification.delay.next.attempt", System.currentTimeMillis());
    }

    @Override
    public void loadVerifiedNumbers() {
        Observable.just("")
                .subscribeOn(Schedulers.computation())
                .map(new Func1<String, Long>() {
                    @Override
                    public Long call(String s) {
                        Realm realm = Realm.getDefaultInstance();
                        try {
                            return realm.where(VerifiedNumber.class).count();
                        } finally {
                            realm.close();
                        }
                    }
                }).flatMap(new Func1<Long, Observable<List<VerifiedNumber>>>() {
            @Override
            public Observable<List<VerifiedNumber>> call(Long lastUpdated) {
                return backend.loadVerifiedNumbers(lastUpdated);
            }
        }).subscribeOn(Schedulers.io())
                .subscribe(new Action1<List<VerifiedNumber>>() {
                    @Override
                    public void call(List<VerifiedNumber> verifiedNumbers) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(verifiedNumbers);
                        realm.commitTransaction();
                    }
                }, new Action1<Throwable>()

                {
                    @Override
                    public void call(Throwable throwable) {
                        PLog.e(TAG, throwable.getMessage(), throwable);
                    }
                });
    }

    static class Holder {
        static UserManager instance = new UserManager(ParseBackend.getInstance());
    }

    public static UserManager getInstance() {
        return Holder.instance;
    }
}
