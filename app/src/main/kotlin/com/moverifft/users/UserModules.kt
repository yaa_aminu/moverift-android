package com.moverifft.users

import com.moverifft.users.data.UserApi
import com.moverifft.users.data.UserManager
import com.moverifft.users.net.ParseBackend
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by yaaminu on 6/8/17.
 */
@Module
class UserModules {
    @Provides
    @Singleton
    fun getUserManager(backend: ParseBackend): UserApi {
        return UserManager(backend)
    }

    @Provides
    @Singleton
    fun getParseBackend() = ParseBackend.getInstance()
}