package com.moverifft.common

/**
 * Created by yaaminu on 6/8/17.
 */
object Constants {
    const val PHONE_NUMBER = "phoneNumber"
    const val IS_VERIFIED = "isVerified"
    const val NAME = "name"
    const val TRUCK_TYPE_IDENTIFIER = "truckTypeIdentifier"
    const val OWNER_ID = "ownerId"
    const val REG_NO = "regNo"
    const val AVAILABLE = "available"
    const val TRUCK_VERIFIED = "verified"

    const val LAT_LANG = "latLang"
    const val TIME_REPORTED = "timeReported"
    const val TRUCK_ID = "truckId"
    const val OWNER_NAME = "ownerName"
    const val RATING = "rating"


    const val HIRED_BY = "hiredBy"
    const val TRIP_END_TIME = "endTime"
    const val START_LOCATION = "startLocation"
    const val END_LOCATION = "endLocation"
    const val USER_ID = "userId"
    const val INITIAL_TRUCK_LOCATION = "initialTruckLocation"
    const val EMAIL_ADDRESS = "email_address"
}