package com.moverifft

import com.moverifft.ui.clients.*
import com.moverifft.ui.common.LauncherActivity
import com.moverifft.ui.driver.LoginActivity
import com.moverifft.ui.driver.TripsTabFragment
import com.moverifft.users.data.UserManager
import com.moverifft.users.net.PendingOrderFragment
import com.moverifft.utils.common.PushProcessor

/**
 * Created by yaaminu on 6/8/17.
 */
@dagger.Component(modules = [(com.moverifft.users.UserModules::class), (TruckModules::class), (TripsModules::class), (com.moverifft.MoveRiftAppModule::class)])
@javax.inject.Singleton
interface MoveRifftGraph {
    fun inject(application: com.moverifft.MoveRifft)
    fun inject(tripsTabFragment: TripsTabFragment)
    fun inject(infoFragment: InfoFragment)
    fun inject(bookTruckFragment: BookTruckFragment)
    fun inject(tripsActivity: TripsActivity)
    fun inject(tripsFragment: TripsFragment)
    fun inject(tripDetailsActivity: TripDetailsActivity)
    fun inject(pushProcessor: PushProcessor)
    fun inject(loginActivity: LoginActivity)
    fun inject(launcherActivity: LauncherActivity)
    fun inject(clientMainActivity: ClientMainActivity)
    fun inject(userManager: UserManager)
    fun inject(pendingOrderFragment: PendingOrderFragment)
}