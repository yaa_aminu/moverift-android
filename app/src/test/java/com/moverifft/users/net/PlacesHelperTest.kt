package com.moverifft.users.net

import com.moverifft.ui.clients.LatLang
import com.moverifft.utils.PLog
import org.junit.Test
import rx.schedulers.Schedulers

/**
 * Created by yaaminu on 1/10/18.
 */
class PlacesHelperTest {

    @Test
    fun searchPlace() {
        PLog.setLogLevel(PLog.LEVEL_NONE)
        PlacesHelper.searchPlace("", LatLang(5.5905595, -0.2502697))
                .subscribeOn(Schedulers.immediate())
                .subscribe {
                    println(it)
                }
    }

}